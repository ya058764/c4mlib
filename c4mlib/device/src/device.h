/**
 * @file common.h
 * @author LiYu87
 * @date 2019.02.22
 * @brief
 *
 * @priority 0
 */
#ifndef C4MLIB_DEVICE_DEVICE_H
#define C4MLIB_DEVICE_DEVICE_H

#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>
#include <util/delay.h>

/* Public Section Start */
void ASA_DEVICE_set(void);
/* Public Section End */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/device/src/config_str.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/stdio/src/stdio.h"

#include <stdbool.h>

// Global configuration structure
extern TypeOfASAConfigStr ASAConfigStr_inst;

#endif  // C4MLIB_DEVICE_DEVICE_H
