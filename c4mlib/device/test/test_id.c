// TODO add asa_id test here

#include "c4mlib/device/src/device.h"

int main() {
    ASA_STDIO_init();

    uint8_t pridect_0 = 0x00;
    uint8_t pridect_1 = 0xE0;
    printf("Common test - test_id\n");
    printf("befor ASABUS_ID_init: %s\n",
           (DDRB == pridect_0 ? "TRUE" : "FALSE"));
    printf("    DDRB = %d\n", DDRB);
    printf("    predict DDRB = %d\n", pridect_0);

    ASABUS_ID_init();

    printf("After ASABUS_ID_init: %s\n",
           (DDRB == pridect_1 ? "TRUE" : "FALSE"));
    printf("    DDRB = %d\n", DDRB);
    printf("    predict DDRB = %d\n", pridect_0);

    return 0;
}
