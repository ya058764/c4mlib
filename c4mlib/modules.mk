##
# @file modules.mk
# @author LiYu87
# @date 2019.01.28
# @brief 提供變數MODULES模組列表供其他makefile使用
#
# 將 MODULES 變數集中管理
# 因為皆是同一個專案，在測試時因將所有模組引入並測試
# 所以 MODULES 應包含所有模組

MODULES  = hardware
MODULES += device
MODULES += stdio
MODULES += asabus
MODULES += asahmi
MODULES += asatwi
MODULES += asauart
MODULES += asaspi
MODULES += interrupt
MODULES += time
