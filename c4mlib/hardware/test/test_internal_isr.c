/**
 * @file test_internal_isr.c
 * @author LiYu87
 * @date 2019.01.28
 * @brief 測試能否正常使用 INTERNAL_ISR
 *
 * 輸出應為 INTERNAL_ISR
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/isr.h"

volatile uint16_t count;
volatile uint16_t s;

int main() {
    ASA_STDIO_init();
    printf("start test of INTERNAL_ISR\n");

    TCCR2 |= (1 << WGM21) | (1 << CS21) | (1 << CS20);
    OCR2 = 80;
    TIMSK |= (1 << OCIE2);
    printf("start! \n");

    uint16_t pre_s = s;
    sei();
    while (1) {
        if (pre_s == s) {
            continue;
        }
        else {
            pre_s = s;
            printf("count = %d\n", s);
        }
    }
    return 0;
}

// INTERNAL_ISR(TIMER2_COMP_vect) {
//     printf("INTERNAL_ISR\n");
// }
