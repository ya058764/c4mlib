#ifndef C4MLIB_HARDWARE_HARDWARE_H
#define C4MLIB_HARDWARE_HARDWARE_H

#include "acomp.h"
#include "adc.h"
#include "dio.h"
#include "ext.h"
#include "hal_spi.h"
#include "hal_uart.h"
#include "isr.h"
#include "spi.h"
#include "tim.h"
#include "twi.h"
#include "uart.h"

#endif  // C4MLIB_HARDWARE_HARDWARE_H
