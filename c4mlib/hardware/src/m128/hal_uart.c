/**
 * @file hal_uart.h
 * @author Ye cheng-Wei
 * @date 2019.1.22
 * @brief Implement a hardware abstraction layer for atmega128.
 */

#include "c4mlib/hardware/src/hal_uart.h"

#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/time/src/hal_time.h"

#include <avr/io.h>
#include <stddef.h>

static void M128_UART0_write_byte(uint8_t data);
static void M128_UART0_init();
static void M128_UART0_write_multi_bytes(uint8_t *data_p, uint8_t sz_data);
static void M128_UART0_read_byte(uint8_t *data_p);
static void M128_UART0_read_bytes(uint8_t *data_p, uint8_t sz_data);

static void M128_UART1_init();
static void M128_UART1_write_byte(uint8_t data);
static void M128_UART1_write_multi_bytes(uint8_t *data_p, uint8_t sz_data);
static void M128_UART1_read_byte(uint8_t *data_p);
static void M128_UART1_read_bytes(uint8_t *data_p, uint8_t sz_data);

static uint8_t UART0_error_code;
static uint8_t UART1_error_code;

TypeOfUARTInst UART0_Inst = {.init = M128_UART0_init,
                             .tx_compelete_cb = 0,
                             .rx_compelete_cb = 0,
                             .write_byte = M128_UART0_write_byte,
                             .write_multi_bytes = M128_UART0_write_multi_bytes,
                             .read_byte = M128_UART0_read_byte,
                             .read_multi_bytes = M128_UART0_read_bytes,
                             .error_code = &UART0_error_code};

TypeOfUARTInst UART1_Inst = {.init = M128_UART1_init,
                             .tx_compelete_cb = 0,
                             .rx_compelete_cb = 0,
                             .write_byte = M128_UART1_write_byte,
                             .write_multi_bytes = M128_UART1_write_multi_bytes,
                             .read_byte = M128_UART1_read_byte,
                             .read_multi_bytes = M128_UART1_read_bytes,
                             .error_code = &UART1_error_code};

INTERNAL_ISR(USART0_TX_vect) {
    if (UART0_Inst.tx_compelete_cb == NULL)
        return;
    else
        UART0_Inst.tx_compelete_cb();
}

INTERNAL_ISR(USART0_RX_vect) {
    if (UART0_Inst.rx_compelete_cb == NULL)
        return;
    else
        UART0_Inst.rx_compelete_cb();
}

INTERNAL_ISR(USART1_TX_vect) {
    if (UART1_Inst.tx_compelete_cb == NULL)
        return;
    else
        UART1_Inst.tx_compelete_cb();
}

INTERNAL_ISR(USART1_RX_vect) {
    if (UART1_Inst.rx_compelete_cb == NULL)
        return;
    else
        UART1_Inst.rx_compelete_cb();
}

void M128_UART0_init() {
    // Initialize the UART0 with 38400 Baudrate
    uint16_t baud = F_CPU / 16 / DEFAULTUARTBAUD - 1;
    UBRR0H = (unsigned char)(baud >> 8);
    UBRR0L = (unsigned char)baud;

    UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
    UCSR0C |= (3 << UCSZ00);

    // Enable RX interrupt
    UCSR0B |= (1 << RXCIE0);
}

void M128_UART0_write_byte(uint8_t data) {
    while (!(UCSR0A & (1 << UDRE0)))
        ;  // Wait for UDR empty
    UDR0 = data;
    UART0_error_code = 0;
    return;
}

void M128_UART0_write_multi_bytes(uint8_t *data_p, uint8_t sz_data) {
    for (int i = 0; i < sz_data; i++) {
        while (!(UCSR0A & (1 << UDRE0)))
            ;  // Wait for UDR empty
        UDR0 = data_p[i];
    }
    UART0_error_code = 0;
    return;
}

void M128_UART0_read_byte(uint8_t *data_p) {
    HAL_Time expire_time;
    HAL_get_expired_time_ms(50UL,
                            &expire_time);  // Get current time tick (Unit: 1ms)
    while (
        !(UCSR0A & (1 << RXC0)))  // If there is no RX complete flag, Block here
    {
        if (HAL_timeout_test(expire_time)) {  // Check timeout

            UART0_error_code = HAL_ERROR_TIMEOUT;
            data_p[0] = 0;
            return;
        }
    }
    data_p[0] = UDR0;
    UART0_error_code = 0;
    return;
}

void M128_UART0_read_bytes(uint8_t *data_p, uint8_t sz_data) {
    HAL_Time expire_time;
    for (int i = 0; i < sz_data; i++) {
        HAL_get_expired_time_ms(
            50UL, &expire_time);  // Get current time tick (Unit: 1ms)

        while (!(UCSR0A &
                 (1 << RXC0)))  // If there is no RX complete flag, Block here
        {
            if (HAL_timeout_test(expire_time)) {  // Check timeout

                UART0_error_code = HAL_ERROR_TIMEOUT;
                data_p[i] = 0;
                return;
            }
        }
        data_p[i] = UDR0;
    }
    UART0_error_code = 0;
    return;
}

void M128_UART1_init() {
    // Initialize the UART1 with 38400 Baudrate
    uint16_t baud = F_CPU / 16 / DEFAULTUARTBAUD - 1;
    UBRR1H = (unsigned char)(baud >> 8);
    UBRR1L = (unsigned char)baud;

    UCSR1B |= (1 << RXEN1) | (1 << TXEN1);
    UCSR1C |= (3 << UCSZ10);

    // Enable RX interrupt
    UCSR1B |= (1 << RXCIE1);
}

void M128_UART1_write_byte(uint8_t data) {
    while (!(UCSR1A & (1 << UDRE1)))
        ;  // Wait for UDR empty
    UDR1 = data;
    UART1_error_code = 0;
}

void M128_UART1_write_multi_bytes(uint8_t *data_p, uint8_t sz_data) {
    for (int i = 0; i < sz_data; i++) {
        while (!(UCSR1A & (1 << UDRE1)))
            ;  // Wait for UDR empty
        UDR1 = data_p[i];
    }
    UART1_error_code = 0;
}

void M128_UART1_read_byte(uint8_t *data_p) {
    HAL_Time expire_time;
    HAL_get_expired_time_ms(50UL,
                            &expire_time);  // Get current time tick (Unit: 1ms)
    while (
        !(UCSR1A & (1 << RXC1)))  // If there is no RX complete flag, Block here
    {
        if (HAL_timeout_test(expire_time)) {  // Check timeout
            UART1_error_code = HAL_ERROR_TIMEOUT;
            data_p[0] = 0;
            return;
        }
    }
    data_p[0] = UDR1;
    UART1_error_code = 0;
    return;
}

void M128_UART1_read_bytes(uint8_t *data_p, uint8_t sz_data) {
    HAL_Time expire_time;
    for (int i = 0; i < sz_data; i++) {
        HAL_get_expired_time_ms(
            50UL, &expire_time);  // Get current time tick (Unit: 1ms)
        while (!(UCSR1A &
                 (1 << RXC1)))  // If there is no RX complete flag, Block here
        {
            if (HAL_timeout_test(expire_time)) {  // Check timeout
                UART1_error_code = HAL_ERROR_TIMEOUT;
                data_p[i] = 0;
                return;
            }
        }
        data_p[i] = UDR1;
    }
    UART1_error_code = 0;
    return;
}
