#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/interrupt/src/extint.h"
#include "c4mlib/interrupt/src/timint.h"

INTERNAL_ISR(TIMER0_COMP_vect) {
    // Make sure there is initialize, Must Call TimInt_init() first!
    if (RedirectTableOfTimIntStr[0] == NULL) {
        return;
    }
    TimInt_step(RedirectTableOfTimIntStr[0]);
}

INTERNAL_ISR(TIMER1_COMPA_vect) {
    // Make sure there is initialize, Must Call TimInt_init() first!
    if (RedirectTableOfTimIntStr[1] == NULL) {
        return;
    }
    TimInt_step(RedirectTableOfTimIntStr[1]);
}

INTERNAL_ISR(TIMER2_COMP_vect) {
    // Make sure there is initialize, Must Call TimInt_init() first!
    if (RedirectTableOfTimIntStr[2] == NULL) {
        return;
    }
    TimInt_step(RedirectTableOfTimIntStr[2]);
}

INTERNAL_ISR(TIMER3_COMPA_vect) {
    // Make sure there is initialize, Must Call TimInt_init() first!
    if (RedirectTableOfTimIntStr[3] == NULL) {
        return;
    }
    TimInt_step(RedirectTableOfTimIntStr[3]);
}

INTERNAL_ISR(INT0_vect) {
    // Make sure there is initialize, Must Call ExtInt_init() first!
    if (RedirectTableOfExtIntStr[0] == NULL) {
        return;
    }
    ExtInt_step(RedirectTableOfExtIntStr[0]);
}

INTERNAL_ISR(INT1_vect) {
    // Make sure there is initialize, Must Call ExtInt_init() first!
    if (RedirectTableOfExtIntStr[1] == NULL) {
        return;
    }
    ExtInt_step(RedirectTableOfExtIntStr[1]);
}
INTERNAL_ISR(INT2_vect) {
    // Make sure there is initialize, Must Call ExtInt_init() first!
    if (RedirectTableOfExtIntStr[2] == NULL) {
        return;
    }
    ExtInt_step(RedirectTableOfExtIntStr[2]);
}

INTERNAL_ISR(INT3_vect) {
    // Make sure there is initialize, Must Call ExtInt_init() first!
    if (RedirectTableOfExtIntStr[3] == NULL) {
        return;
    }
    ExtInt_step(RedirectTableOfExtIntStr[3]);
}

INTERNAL_ISR(INT4_vect) {
    // Make sure there is initialize, Must Call ExtInt_init() first!
    if (RedirectTableOfExtIntStr[4] == NULL) {
        return;
    }
    ExtInt_step(RedirectTableOfExtIntStr[4]);
}

INTERNAL_ISR(INT5_vect) {
    // Make sure there is initialize, Must Call ExtInt_init() first!
    if (RedirectTableOfExtIntStr[5] == NULL) {
        return;
    }
    ExtInt_step(RedirectTableOfExtIntStr[5]);
}

INTERNAL_ISR(INT6_vect) {
    // Make sure there is initialize, Must Call ExtInt_init() first!
    if (RedirectTableOfExtIntStr[6] == NULL) {
        return;
    }
    ExtInt_step(RedirectTableOfExtIntStr[6]);
}

INTERNAL_ISR(INT7_vect) {
    // Make sure there is initialize, Must Call ExtInt_init() first!
    if (RedirectTableOfExtIntStr[7] == NULL) {
        return;
    }
    ExtInt_step(RedirectTableOfExtIntStr[7]);
}
