#include "c4mlib/hardware/src/adc.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

// TODO: HARDWARE ADC 尋找負責人，或安排時程

/* public functions declaration section ------------------------------------- */
char ADC_fpt(char LSByte, char Mask, char Shift, char Data);
char ADC_fgt(char LSByte, char Mask, char Shift, void* Data_p);
char ADC_put(char LSByte, char Bytes, void* Data_p);
char ADC_get(char LSByte, char Bytes, void* Data_p);

char ADC_set(char LSByte, char Mask, char Shift, char Data)
    __attribute__((alias("ADC_fpt")));

/* define section ----------------------------------------------------------- */

/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */

/* static function contents section ----------------------------------------- */

/* public function contents section ----------------------------------------- */
char ADC_fpt(char LSByte, char Mask, char Shift, char Data) {
    if (Shift <= 7) {
        switch (LSByte) {
            case 200:
                REGFPT(ADMUX, Mask, Shift, Data);
                break;
            case 201:
                REGFPT(ADCSRA, Mask, Shift, Data);
                break;
            case 202:
                REGFPT(DDRF, Mask, Shift, Data);
                break;
            default:
                return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_SHIFT;
    }
    return RES_OK;
}

char ADC_fgt(char LSByte, char Mask, char Shift, void* Data_p) {
    if (Shift <= 7) {
        switch (LSByte) {
            case 201:
                REGFGT(ADCSRA, Mask, Shift, Data_p);
                break;
            default:
                return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_SHIFT;
    }
    return RES_OK;
}

char ADC_put(char LSByte, char Bytes, void* Data_p) {
    return RES_OK;
}

char ADC_get(char LSByte, char Bytes, void* Data_p) {
    // TODO: ADMUX bytes error ?
    if (Bytes == 1) {
        switch (LSByte) {
            case 1: {
                *((char*)Data_p) = ADCH;
                break;
            }
            case 2: {
                *((char*)Data_p) = ADCL;
                *((char*)Data_p + 1) = ADCH;
                break;
            }
            default:
                return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_BYTES;
    }
    return RES_OK;
}
