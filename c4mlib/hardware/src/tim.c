#if defined(__AVR_ATmega128__)
#    include "m128/tim.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/tim.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/tim.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
