#ifndef C4MLIB_HARDWARE_ADC_H
#define C4MLIB_HARDWARE_ADC_H

/* Public Section Start */
char ADC_set(char LSByte, char Mask, char Shift, char Data);
char ADC_fpt(char LSByte, char Mask, char Shift, char Data);
char ADC_fgt(char LSByte, char Mask, char Shift, void *Data_p);
char ADC_put(char LSbyte, char Bytes, void *Data_p);
char ADC_get(char LSByte, char Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_ADC_H
