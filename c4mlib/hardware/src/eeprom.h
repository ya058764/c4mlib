#ifndef C4MLIB_HARDWARE_EEPROM_H
#define C4MLIB_HARDWARE_EEPROM_H

/* Public Section Start */
void EEPROM_set(int Address, char Bytes, void *Data_p);
void EEPROM_get(int Address, char Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_EEPROM_H
