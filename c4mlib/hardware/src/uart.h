#ifndef C4MLIB_HARDWARE_UART_H
#define C4MLIB_HARDWARE_UART_H

/* Public Section Start */
char UART_set(char LSByte, char Mask, char Shift, char Data);
char UART_fpt(char LSByte, char Mask, char Shift, char Data);
char UART_fgt(char LSByte, char Mask, char Shift, void *Data_p);
char UART_put(char LSbyte, char Bytes, void *Data_p);
char UART_get(char LSByte, char Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_UART_H
