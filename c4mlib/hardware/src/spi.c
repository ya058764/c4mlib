#if defined(__AVR_ATmega128__)
#    include "m128/spi.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/spi.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/spi.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
