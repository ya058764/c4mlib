#ifndef C4MLIB_HARDWARE_ACOMP_H
#define C4MLIB_HARDWARE_ACOMP_H

/* Public Section Start */
char ACOMP_set(char LSByte, char Mask, char Shift, char Data);
char ACOMP_fpt(char LSByte, char Mask, char Shift, char Data);
char ACOMP_fgt(char LSByte, char Mask, char Shift, void *Data_p);
char ACOMP_put(char LSbyte, char Bytes, void *Data_p);
char ACOMP_get(char LSByte, char Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_ACOMP_H
