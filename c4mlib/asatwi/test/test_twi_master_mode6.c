#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/device/src/device.h"
#define ARRARY_SIZE 10
#define SLA 0x39

void TWI_Master_set();
uint8_t check = 0;
int main(void) {
    ASA_STDIO_init();
    TWI_Master_set();
    uint8_t Mode = 5;
    uint8_t default_SLA = 0b00000000;
    uint8_t regadd2 = 0x02;
    uint8_t regadd3 = 0x03;
    uint8_t set_SLA = SLA;
    uint8_t conf_add = 0b00000000;
    uint8_t bytes = ARRARY_SIZE;
    uint8_t data2 = 9;
    uint8_t data1[ARRARY_SIZE];
    for (int i = 0; i < ARRARY_SIZE; i++) {
        data1[i] = i + 10;
        printf("[Master] Transmit mode5 data1[%d] = %d\t\n", i, data1[i]);
    }
    printf("[Master] Transmit mode5 data2 = %d\n", data2);
    check = TWIM_trm(Mode, default_SLA, conf_add, 1, &set_SLA);
    printf("[Master] Device ID Configer test!!\n");
    check = TWIM_trm(Mode, SLA, regadd2, bytes, data1);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(50);
    check = TWIM_trm(Mode, SLA, regadd3, 1, &data2);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(50);
    check = TWIM_rec(Mode, SLA, regadd2, bytes, data1);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(50);
    check = TWIM_rec(Mode, SLA, regadd3, 1, &data2);
    printf("[Master] Error code : %d\n", check);
    for (int i = 0; i < bytes; i++) {
        printf("[Master] Receive mode5 data1[%d] = %d\t\n", i, data1[i]);
    }
    printf("[Master] Receive mode4 data2 = %d\n", data2);
}
void TWI_Master_set() {
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
}
