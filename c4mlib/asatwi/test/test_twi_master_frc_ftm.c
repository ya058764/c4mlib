#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/device/src/device.h"
#define ARRARY_SIZE 10
#define SLA 0x39
#define Test_mode 4

void TWI_Master_set();
char check = 0;

int main(void) {
    ASA_STDIO_init();
    TWI_Master_set();

    uint8_t regadd2 = 0x02;
    uint8_t regadd3 = 0x03;
    uint8_t test_data1 = 0x0f;
    uint8_t test_data2 = 0xf0;
    uint8_t temp_data1 = 0x05;
    uint8_t temp_data2 = 0x50;
    uint8_t ans1 = 0;
    uint8_t ans2 = 0;
    check = TWIM_trm(Test_mode, SLA, regadd2, 1, &test_data1);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_trm(Test_mode, SLA, regadd3, 1, &test_data2);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_ftm(Test_mode, SLA, regadd2, 0x0f, 0, &temp_data1);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_ftm(Test_mode, SLA, regadd3, 0xf0, 0, &temp_data2);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_frc(Test_mode, SLA, regadd2, 0x0f, 0, &ans1);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_frc(Test_mode, SLA, regadd3, 0xf0, 0, &ans2);
    printf("[Master] Transmit Check = %d\n", check);
    printf("[Master] Tramsmit frc = %x\n", ans1);
    printf("[Master] Tramsmit frc = %x\n", ans2);
}
void TWI_Master_set() {
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
}
