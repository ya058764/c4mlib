#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/device/src/device.h"
#define ARRARY_SIZE 11
#define SLA 0x39

void TWI_Master_set();
char check = 0;
int main(void) {
    ASA_STDIO_init();
    TWI_Master_set();
    uint8_t data1[ARRARY_SIZE];
    uint8_t bits1 = 0;
    for (int i = ARRARY_SIZE - 2; i >= 0; --i) {
        data1[i] = i + 10;
        printf("[MASTER] Transmit mode2 data1[%d] = %d\t\n", i, data1[i]);
    }
    for (int i = ARRARY_SIZE - 2; i >= 0; --i) {
        bits1 = data1[i] & 0xE0;
        data1[i] &= (~0xE0);
        data1[i] <<= 3;
        data1[i + 1] |= bits1 >> 5;
    }
    uint8_t Mode = 1;
    uint8_t regadd1 = 0x02;
    uint8_t bytes = ARRARY_SIZE;
    check = TWIM_trm(Mode, SLA, regadd1, bytes, data1);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(150);
    check = TWIM_rec(Mode, SLA, regadd1, bytes, data1);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(150);
    for (int i = 0; i < bytes; i++) {
        printf("[Master] Receive mode2 data1[%d] = %d\t\n", i, data1[i]);
    }
}
void TWI_Master_set() {
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
}
