#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/asatwi/src/twi.h"
#include "c4mlib/device/src/device.h"

#include <avr/interrupt.h>

#include "c4mlib/config/remo_reg.cfg"

void TWI_Slave_set();

int main(void) {
    ASA_STDIO_init();
    // Slave Setting
    ASA_DEVICE_set();
    TWI_Slave_set();
    TypeOfSerialIsr TWIIsrStr = SERIAL_ISR_STR_TWI_INI;
    // FIXME: 使用新版初始化
    SerialIsr_init(&TWIIsrStr, 0);
    uint8_t reg_1[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    uint8_t reg_2[1] = {0};
    uint8_t reg_1_ID = RemoRW_reg(&TWIIsrStr, reg_1, 10);
    uint8_t reg_2_ID = RemoRW_reg(&TWIIsrStr, reg_2, 1);
    printf("Create RemoRWreg [%u] with %u bytes\n", reg_1_ID,
           TWIIsrStr.remo_reg[reg_1_ID].sz_reg);
    printf("Create RemoRWreg [%u] with %u bytes\n", reg_2_ID,
           TWIIsrStr.remo_reg[reg_2_ID].sz_reg);
    printf("DEVICE ID = %x\n", TWAR);
    sei();
    while (1) {
        if (reg_1[9] == 19) {
            for (int i = 0; i < 10; i++) {
                printf("[Slave] Receive mode4 Reg_1[%d]=%d\t\n", i, reg_1[i]);
                reg_1[i] += 5;
            }
        }
        if (reg_2[0] == 9) {
            printf("[Slave] Receive mode4 Reg_2[0]=%d\n", reg_2[0]);
            reg_2[0] += 5;
        }
    }
}
ISR(TWI_vect) {
    TWI5_isr();
}

void TWI_Slave_set() {
    TWAR = (ASAConfigStr_inst.ASA_ID |= 0b00000001);
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
    // enable TWI TWI_Interrupt and shack_hand for Master Start signal
    TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWEA);
}
