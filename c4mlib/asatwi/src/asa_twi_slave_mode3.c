#if defined(__AVR_ATmega128__)
#    include "m128/asa_twi_slave_mode3.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/asa_twi_slave_mode3.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/asa_twi_slave_mode3.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
