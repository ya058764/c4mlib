/**
 * @file asa_twi_slave_mode1.c
 * @author Yuchen
 * @brief Slave TWI mode5 通訊封包函式實現
 * @date 2019-03-29
 *
 * @copyright Copyright (c) 2019
 *
 */

#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"

void TWI6_isr(void) {
    switch (TWI_STATUS) {
        case TWI_SR_SLA_ACK: {
            DEBUG_INFO("Own SLA+W has been received---%x\n", TWI_STATUS);
            ASATWISerialIsrStr->sm_status = TWI_SR_SLA_ACK;
            TWICom_ACKCom(USE_ACK);
            break;
        }
        case TWI_REP_START: {
            DEBUG_INFO(
                "Previously addressed with own SLA+W data has been "
                "received---%x\n",
                TWI_STATUS);
            ASATWISerialIsrStr->sm_status = TWI_REP_START;
            TWICom_ACKCom(USE_ACK);
            break;
        }
        case TWI_SR_SLA_Gen_ACK: {
            //收到廣播頻道的SLA
            DEBUG_INFO(
                "General call address has been received ACK has been "
                "returned---%x\n",
                TWI_STATUS);
            ASATWISerialIsrStr->sm_status = TWI_SR_SLA_Gen_ACK;
            TWICom_ACKCom(USE_ACK);
            break;
        }
        case TWI_SR_DATA_Gen_ACK: {
            //接收廣播頻道SLA的DATA
            DEBUG_INFO(
                "General call DATA has been received ACK has been "
                "returned---%x\n",
                TWI_STATUS);
            //讀取資料存入ID = 0之暫存器(Device ID)
            if (ASATWISerialIsrStr->byte_counter == 0) {
                ASATWISerialIsrStr->reg_address = TWDR;
            }
            else {
                ASATWISerialIsrStr->remo_reg[ASATWISerialIsrStr->reg_address]
                    .data_p[0] = (TWDR & 0xfE);
                //將暫存器ID改為1
                ASATWISerialIsrStr->reg_address += 1;
                //對ID = 1之暫存器寫入1，準備對EEPROM寫入新Device ID
                ASATWISerialIsrStr->remo_reg[ASATWISerialIsrStr->reg_address]
                    .data_p[0] = 1;
                DEBUG_INFO("ctl_id=%x\n",
                           ASATWISerialIsrStr
                               ->remo_reg[ASATWISerialIsrStr->reg_address]
                               .data_p[ASATWISerialIsrStr->byte_counter]);
            }
            ASATWISerialIsrStr->byte_counter++;
            ASATWISerialIsrStr->sm_status = TWI_SR_DATA_Gen_ACK;
            TWICom_ACKCom(USE_ACK);
            break;
        }
        case TWI_SR_DATA_ACK: {
            DEBUG_INFO(
                "Previously addressed with own SLA+W data has been "
                "received---%x\n",
                TWI_STATUS);
            if (ASATWISerialIsrStr->byte_counter == 0) {
                ASATWISerialIsrStr->reg_address = TWDR;
                ASATWISerialIsrStr->byte_counter =
                    (ASATWISerialIsrStr
                         ->remo_reg[ASATWISerialIsrStr->reg_address]
                         .sz_reg);
                DEBUG_INFO("ASATWISerialIsrStr->byte_counter=%d\n",
                           ASATWISerialIsrStr->byte_counter);
            }
            else {
                ASATWISerialIsrStr->byte_counter--;
                ASATWISerialIsrStr->remo_reg[ASATWISerialIsrStr->reg_address]
                    .data_p[ASATWISerialIsrStr->byte_counter] = TWDR;
            }
            ASATWISerialIsrStr->sm_status = TWI_SR_DATA_ACK;
            TWICom_ACKCom(USE_ACK);
            break;
        }
        case TWI_SR_DATA_STO: {
            DEBUG_INFO(
                "A STOP condition or repeated START condition has been "
                "received---%x\n",
                TWI_STATUS);
            //若前狀態為廣播接收則對EEPROM直寫ID = 0 之暫存器的新Device id
            if (ASATWISerialIsrStr->sm_status == TWI_SR_DATA_Gen_ACK) {
                if (ASATWISerialIsrStr
                        ->remo_reg[ASATWISerialIsrStr->reg_address]
                        .func_p != NULL)
                    ASATWISerialIsrStr
                        ->remo_reg[ASATWISerialIsrStr->reg_address]
                        .func_p(ASAUARTSerialIsrStr
                                    ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                                    .funcPara_p);
            }
            TWICom_ACKCom(USE_ACK);
            ASATWISerialIsrStr->byte_counter = 0;
            break;
        }
        case TWI_ST_SLA_ACK: {
            DEBUG_INFO("Own SLA+R has been received---%x\n", TWI_STATUS);
            //第一筆資料傳送
            ASATWISerialIsrStr->byte_counter =
                (ASATWISerialIsrStr->remo_reg[ASATWISerialIsrStr->reg_address]
                     .sz_reg) -
                1;
            TWDR = ASATWISerialIsrStr->remo_reg[ASATWISerialIsrStr->reg_address]
                       .data_p[ASATWISerialIsrStr->byte_counter];
            //若只傳送一筆資料，傳送完畢後直送NACK
            if (ASATWISerialIsrStr->byte_counter == 0) {
                ASATWISerialIsrStr->byte_counter = 0;
                TWICom_ACKCom(USE_NACK);
            }
            else {
                //若傳送資料不只有一筆，直送ACK
                TWCR |= (1 << TWEN) | (1 << TWEA) | (1 << TWIE);
            }
            break;
        }
        case TWI_ST_DATA_ACK: {
            DEBUG_INFO("Data byte in TWDR has been transmitted---%x\n",
                       TWI_STATUS);
            ASATWISerialIsrStr->byte_counter--;
            if (ASATWISerialIsrStr->byte_counter == 0) {
                //最後一筆資料
                //傳送最後一筆資料 + NACK
                TWDR = ASATWISerialIsrStr
                           ->remo_reg[ASATWISerialIsrStr->reg_address]
                           .data_p[ASATWISerialIsrStr->byte_counter];
                TWICom_ACKCom(USE_NACK);
            }
            else {
                //資料尚未傳送完畢
                //傳送下一筆資料 + ACK
                TWDR = ASATWISerialIsrStr
                           ->remo_reg[ASATWISerialIsrStr->reg_address]
                           .data_p[ASATWISerialIsrStr->byte_counter];
                TWICom_ACKCom(USE_ACK);
            }
            break;
        }
        case TWI_ST_Data_last: {
            DEBUG_INFO("Last data byte in TWDR has been transmitted---%x\n",
                       TWI_STATUS);
            //最後一筆資料以傳遞完畢
            TWICom_ACKCom(USE_ACK);
            break;
        }
        case TWI_ST_DATA_NACK: {
            DEBUG_INFO("Data byte in TWDR has been transmitted---%x\n",
                       TWI_STATUS);
            TWICom_ACKCom(USE_NACK);
            break;
        }
    }
}
