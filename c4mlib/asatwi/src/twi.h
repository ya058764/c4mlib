/**
 * @file twi.h
 * @author Yuchen
 * @brief 放置TWI通訊封包通用函式及Macro
 * @date 2019-03-27
 *
 * @copyright Copyright (c) 2019
 *
 */
#ifndef TWI_H
#define TWI_H

#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>

#define TIMEOUTSETTING 500000 /**< TWI逾時設定 */
#define CF_BIT 3              /**< TWI Contral Flag bit數設定 */
#define CF_MASK 0xE0          /**< TWI Contral Flag 遮罩 */
#define TWAR_MASK 0xFE        /**< TWI TWAR 遮罩 */
#define TIMEOUT_FLAG 0X01     /**< TWI 逾時期標 */
#define REG_MAX_COUNT 20      /**< TWI 暫存計數器最大值 */
#define BUFF_MAX_SZ 32        /**< TWI BUFFER最大值 */
/*TWI 狀態列表 */
/*status codes check*/
#define TWI_STATUS ((TWSR) & (0xF8))
//--------------------------------------------------------------------------------------------------------------------
/*status codes for SLA*/
#define SLA_type_error 0xD0
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for start and restart condition*/
#define TWI_START 0x08
#define TWI_REP_START 0x10
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for master transmitter mode*/
#define TWI_MT_SLA_ACK 0x18
#define TWI_MT_SLA_NACK 0x20
#define TWI_MT_DATA_ACK 0x28
#define TWI_MT_DATA_NACK 0x30
#define TWI_MT_ARB_LOST 0x38
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for master receiver mode*/
#define TWI_MR_SLA_ACK 0x40
#define TWI_MR_SLA_NACK 0x48
#define TWI_MR_DATA_ACK 0x50
#define TWI_MR_DATA_NACK 0x58
#define TWI_MR_ARB_LOST 0x38
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for Slave transmitter mode*/
#define TWI_ST_SLA_ACK 0xA8
#define TWI_ST_SLA_Arb_lose_ACK 0xB0
#define TWI_ST_DATA_ACK 0xB8
#define TWI_ST_DATA_NACK 0xC0
#define TWI_ST_Data_last 0xC8
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for Slave receiver mode*/
#define TWI_SR_SLA_ACK 0x60
#define TWI_SR_SLA_Arb_lose_ACK 0x68
#define TWI_SR_SLA_Gen_ACK 0x70
#define TWI_SR_SLA_ArbLos_Gen_ACK 0x78
#define TWI_SR_DATA_ACK 0x80
#define TWI_SR_DATA_NACK 0x88
#define TWI_SR_DATA_Gen_ACK 0x90
#define TWI_SR_DATA_Gen_NACK 0x98
#define TWI_SR_DATA_STO 0xA0
/*ACK status*/
#define USE_ACK 0x01
#define USE_NACK 0x00
//--------------------------------------------------------------------------------------------------------------------
/**
 * @brief TWI Hardware 送訊
 *
 * @param reg   送訊資料
 * @return 1:       Timeout
 *         0xXX:    參考TWI狀態列表
 */
uint8_t TWI_Reg_tram(uint8_t reg);
/**
 * @brief TWI Hardware 收訊
 *
 * @param data  讀取資料位址指標
 * @return 1:       Timeout
 *         0xXX:    參考TWI狀態列表
 */
uint8_t TWI_Reg_rec(uint8_t *data);
/**
 * @brief TWI Hardware Acknowledge
 *
 * @param ack_p Acknowledge Flag
 */
void TWICom_ACKCom(uint8_t ack_p);
/**
 * @brief TWI Hardware Stop
 *
 * @param stop_p    Stop Flag
 */
void TWICom_Stop(uint8_t stop_p);
/**
 * @brief TWI Hardware Start
 *
 * @param stop_p    Stop Flag
 */
uint8_t TWICom_Start(uint8_t _start);
#endif
