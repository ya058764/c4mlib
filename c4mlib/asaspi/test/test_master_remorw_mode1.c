#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

#define SPI_MODE 1
#define ASAID 4

// If control flag bit is three
#define testCF 0x3
#define CF_reg (0x02 << 3)
#define trm_data 87

uint8_t test_trm1[2] = {trm_data, CF_reg};

int main() {
    // Setup
    ASA_STDIO_init();
    printf("Start master mode 1\n");
    SPIM_Inst.init();
    sei();
    while (true) {
        char chk;

        /* Test write remote register SPI mode 1 */
        printf("==== remo one ====\n");
        chk = ASA_SPIM_trm(SPI_MODE, ASAID, testCF, sizeof(test_trm1),
                           &test_trm1[0]);
        printf("chk===%d\n", chk);

        test_trm1[0]++;

        _delay_ms(3000);
    }
}
