#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

#define SPI_MODE 6
#define ASAID 4

void init_timer(void);

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

uint8_t test_trm1[1] = {87};
uint8_t test_trm2[2] = {1, 2};
uint8_t test_trm3[4] = {15, 16, 17, 18};

uint8_t test_rec1[1] = {0};
uint8_t test_rec2[2] = {0, 0};
uint8_t test_rec3[4] = {0, 0, 0, 0};

int main() {
    // Setup
    ASA_STDIO_init();
    HAL_time_init();
    init_timer();
    printf("Start master mode 5\n");
    SPIM_Inst.init();
    sei();
    char er_flag = false;
    char ok_flag = 0;
    while (true) {
        char chk;
        /* Test write remote register SPI mode 0 */
        printf("==== remo one ====\n");
        chk =
            ASA_SPIM_trm(SPI_MODE, ASAID, 2, sizeof(test_trm1), &test_trm1[0]);
        printf("chk===%d\n", chk);
        printf("==== remo two ====\n");
        chk =
            ASA_SPIM_trm(SPI_MODE, ASAID, 3, sizeof(test_trm2), &test_trm2[0]);
        printf("chk===%d\n", chk);
        printf("==== remo four ====\n");
        chk =
            ASA_SPIM_trm(SPI_MODE, ASAID, 4, sizeof(test_trm3), &test_trm3[0]);
        printf("chk===%d\n", chk);

        /* Test read remote register SPI mode 0 */
        printf("==== remo one ====\n");
        chk =
            ASA_SPIM_rec(SPI_MODE, ASAID, 2, sizeof(test_rec1), &test_rec1[0]);
        printf("chk===%d\n", chk);
        for (uint8_t i = 0; i < sizeof(test_rec1); i++) {
            printf("rec1[%d]: %d, ", i, test_rec1[i]);
            if (test_rec1[i] == test_trm1[i]) {
                ok_flag++;
            }
            else {
                er_flag = true;
            }
            test_trm1[i]++;
        }
        printf("\n");
        printf("==== remo two ====\n");
        chk =
            ASA_SPIM_rec(SPI_MODE, ASAID, 3, sizeof(test_rec2), &test_rec2[0]);
        printf("chk===%d\n", chk);
        for (uint8_t i = 0; i < sizeof(test_rec2); i++) {
            printf("rec2[%d]: %d, ", i, test_rec2[i]);
            if (test_rec2[i] == test_trm2[i]) {
                ok_flag++;
            }
            else {
                er_flag = true;
            }
            test_trm2[i] += 2;
        }
        printf("\n");
        printf("==== remo four ====\n");
        chk =
            ASA_SPIM_rec(SPI_MODE, ASAID, 4, sizeof(test_rec3), &test_rec3[0]);
        printf("chk===%d\n", chk);
        for (uint8_t i = 0; i < sizeof(test_rec3); i++) {
            printf("rec3[%d]: %d, ", i, test_rec3[i]);
            if (test_rec3[i] == test_trm3[i]) {
                ok_flag++;
            }
            else {
                er_flag = true;
            }
            test_trm3[i] += 4;
        }
        printf("\n");

        if (er_flag) {
            printf("Test master remo-reg fail !!!\n");
            // while(1);
        }
        else {
            printf("Succeed --> %d\n", ok_flag);
        }
        if (ok_flag >= 35) {
            printf("Test master mode 5 remo-reg succeed !!!\n");
            HAL_delay(2000);
            break;
        }
        HAL_delay(3000UL);
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
