#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/common/src/common.h"
#include "c4mlib/common/src/hal_time.h"
#include "c4mlib/hardware/src/eeprom.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define SPI_MODE 3

int main() {
    // Setup
    ASA_STDIO_init();
    printf("Start frc slave mode 3\n");

    // Initailize Serial SPI remote register type
    TypeOfSerialIsr SPIIsrStr = SERIAL_ISR_STR_SPI_INI;
    SerialIsr_init(&SPIIsrStr, SPIS3_cb);

    uint8_t reg_1[1] = {0};
    uint8_t reg_2[1] = {0};

    uint8_t reg_1_Id = RemoRW_reg(&SPIIsrStr, reg_1, 1);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_1_Id,
           SPIIsrStr.remo_reg[reg_1_Id].sz_reg);

    uint8_t reg_2_Id = RemoRW_reg(&SPIIsrStr, reg_2, 1);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_2_Id,
           SPIIsrStr.remo_reg[reg_2_Id].sz_reg);

    SPIIsrStr.remo_reg[reg_1_Id].data_p[0] = 87;
    SPIIsrStr.remo_reg[reg_2_Id].data_p[0] = 255;

    ASASPISerialIsrStr->rw_mode = 1;
    while (true) {
        // Show the register data
    }
}
