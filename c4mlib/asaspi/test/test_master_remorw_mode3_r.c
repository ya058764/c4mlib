#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

#define SPI_MODE 3
#define ASAID 4

void init_timer(void);

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

uint8_t test_rec1[4] = {0, 0, 0, 0};
uint8_t test_ans1[4] = {15, 16, 17, 18};

int main() {
    // Setup
    ASA_STDIO_init();
    HAL_time_init();
    init_timer();
    printf("Start master read, mode 2\n");
    SPIM_Inst.init();
    sei();
    char er_flag = false;
    char ok_flag = 0;
    while (true) {
        char chk;

        /* Test read remote register SPI mode 1 */
        chk =
            ASA_SPIM_rec(SPI_MODE, ASAID, 0, sizeof(test_rec1), &test_rec1[0]);
        printf("chk===%d\n", chk);
        for (uint8_t i = 0; i < sizeof(test_rec1); i++) {
            printf("rec1[%d]: %d", i, test_rec1[i]);
            if (test_rec1[i] == test_ans1[i]) {
                ok_flag++;
            }
            else {
                er_flag = true;
            }
            test_ans1[i] += 4;
        }
        printf("\n");

        if (er_flag) {
            printf("Test master remo-reg fail !!!\n");
            // while(1);
        }
        else {
            printf("Succeed --> %d\n", ok_flag);
        }
        if (ok_flag >= 20) {
            printf("Test master read slave data by remo-reg succeed !!!\n");
            break;
        }
        _delay_ms(3000);
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
