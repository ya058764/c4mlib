// #define USE_C4MLIB_DEBUG
/**
 * _delay_ms 在debug的時候要加，因為printf會造成時間的延遲
 * 如果不加，會造成通訊失敗
 */
#include "asaspi_master.h"

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"

/**
 * @brief ASA Master SPI driver for transmission.
 *
 * @param[in] mode, SPI transformation general mode.
 * @param[in] ASAID, ASA idendity which user can choose ASA slave board.
 * @param[in] RegAdd, control flag or remote R/W register ID.
 * @param[in] Bytes, send data bytes.
 * @param[in] *Data_p, data pointer.
 * @return char Success or not.
 */
char ASA_SPIM_trm(char mode, char ASAID, char RegAdd, char Bytes,
                  void *Data_p) {
    char echo = 0, err = 0, chk = 0;
    SPIM_Inst.enable_cs(ASAID);
    switch (mode) {
        case 0:
            chk = SPIM_Inst.spi_swap((RegAdd | 0x80));  // add+bit7H
            DEBUG_INFO("get=%d\n", chk);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            SPIM_Inst.spi_swap(*((char *)Data_p));  // send frist bytes
            DEBUG_INFO("get=%d\n", chk);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = 1; i < Bytes; i++) {
                echo = SPIM_Inst.spi_swap(
                    *((char *)Data_p + i));  //送出下一筆&拿回前一筆
                DEBUG_INFO("get=%d\n", echo);
                DEBUG_INFO("old=%d\n", (*((char *)Data_p + i - 1)));
                if (echo != (*((char *)Data_p + i - 1)))
                    err = 1;  //檢查前一筆
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            echo = SPIM_Inst.spi_swap(0);  //喚回最後一筆
            DEBUG_INFO("get=%d\n", echo);
            DEBUG_INFO("old=%d\n", (*((char *)Data_p + Bytes - 1)));
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            if (echo != (*((char *)Data_p + Bytes - 1)))
                err = 1;                    //檢查是否吻合
            chk = SPIM_Inst.spi_swap(err);  //送出是否吻合
            DEBUG_INFO("chk=%d\n", chk);

            return chk;
            break;

        case 1:
            for (int i = Bytes - 1; i >= 0; i--) {
                if (i == Bytes - 1)
                    SPIM_Inst.spi_swap(RegAdd |
                                       (*((char *)Data_p + Bytes - 1)));
                else {
                    SPIM_Inst.spi_swap(*((char *)Data_p + i));
                    DEBUG_INFO("swap:%d\n", *((char *)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
                    _delay_ms(20);
#else
                    _delay_ms(1);
#endif
                }
            }
            break;

        case 2:
            for (int i = 0; i < Bytes; i++) {
                if (i == 0)
                    SPIM_Inst.spi_swap(RegAdd | (*((char *)Data_p)));
                else {
                    SPIM_Inst.spi_swap(*((char *)Data_p + i));
                    DEBUG_INFO("swap:%d\n", *((char *)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
                    _delay_ms(20);
#else
                    _delay_ms(1);
#endif
                }
            }
            break;

        case 3:
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
                DEBUG_INFO("swap:%d\n", *((char *)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 4:
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 5:
            SPIM_Inst.spi_swap(RegAdd | 0x80);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 6:
            SPIM_Inst.spi_swap(RegAdd | 0x80);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 7:
            SPIM_Inst.spi_swap((RegAdd << 1) | 1);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = 0; i < Bytes; i++) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 8:
            SPIM_Inst.spi_swap((RegAdd << 1) | 1);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = Bytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((char *)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                // _delay_ms(1);
#endif
            }
            break;

        default:
            return 1;
            break;
    }
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}

/**
 * @brief ASA Master SPI driver for reception.
 *
 * @param[in] mode, SPI transformation general mode.
 * @param[in] ASAID, ASA idendity which user can choose ASA slave board.
 * @param[in] RegAdd, control flag or remote R/W register ID.
 * @param[in] Bytes, recieve data bytes.
 * @param[in] *Data_p, data pointer that will collect the data.
 * @return char Success or not.
 */
char ASA_SPIM_rec(char mode, char ASAID, char RegAdd, char Bytes,
                  void *Data_p) {
    char chk = 0, a = 0;
    SPIM_Inst.enable_cs(ASAID);
    switch (mode) {
        case 0:
            SPIM_Inst.spi_swap(RegAdd);
// slave會檢查所以比較慢，不delay會出錯
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            *((char *)Data_p) = SPIM_Inst.spi_swap(0);  //送0喚回第一筆
            a = *((char *)Data_p);  //把對面給的資料丟回去
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = 1; i < Bytes; i++) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(a);
                a = *((char *)Data_p + i);
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            SPIM_Inst.spi_swap(a);  //丟回最後一筆
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            chk = SPIM_Inst.spi_swap(0);  //接收對面的檢查結果
            return chk;
            break;

        case 1:
            //
            return HAL_ERROR_MODE_SELECT;
            break;

        case 2:
            //
            return HAL_ERROR_MODE_SELECT;
            break;

        case 3:
            SPIM_Inst.spi_swap(0);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = 0; i < Bytes; i++) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
                DEBUG_INFO("rec %d\n", *((char *)Data_p + i));
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 4:
            SPIM_Inst.spi_swap(0);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = Bytes - 1; i >= 0; i--) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 5:
            SPIM_Inst.spi_swap(RegAdd);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = 0; i < Bytes; i++) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 6:
            *((char *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap(RegAdd);
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = Bytes - 1; i >= 0; i--) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 7:
            *((char *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap((RegAdd << 1));
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = 0; i < Bytes; i++) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }
            break;

        case 8:
            *((char *)Data_p + Bytes - 1) = SPIM_Inst.spi_swap((RegAdd << 1));
#ifdef USE_C4MLIB_DEBUG
            _delay_ms(20);
#else
            _delay_ms(1);
#endif
            for (int i = Bytes - 1; i >= 0; i--) {
                *((char *)Data_p + i) = SPIM_Inst.spi_swap(0);
#ifdef USE_C4MLIB_DEBUG
                _delay_ms(20);
#else
                _delay_ms(1);
#endif
            }

            break;

        default:
            return 1;
            break;
    }
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}

/**
 * @brief ASA Master flag recieve, use ASA_SPIM_rec function to recieve data,
 * and do shift, mask. Finally, collect data to Data_p pointer.
 *
 * @param[in] mode, SPI transformation general mode.
 * @param[in] ASAID, ASA idendity which user can choose ASA slave board.
 * @param[in] RegAdd, control flag or remote R/W register ID.
 * @param[in] Mask, recieve data do mask after shift them.
 * @param[in] Shift, recieve data do shift.
 * @param[in] *Data_p, data pointer that will collect the data.
 * @return char Success or not.
 */
char ASA_SPIM_frc(char mode, char ASAID, char RegAdd, char Mask, char Shift,
                  char *Data_p) {
    if ((mode == 1) || (mode == 2) || (mode == 4) || (mode == 6) || (mode == 8))
        return HAL_ERROR_MODE_SELECT;
    char temp = 0;
    SPIM_Inst.enable_cs(ASAID);
    ASA_SPIM_rec(mode, ASAID, RegAdd, 1, &temp);
    *Data_p = (temp << Shift) & Mask;
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}

/**
 * @brief ASA Master flag transmit, use ASA_SPIM_rec and ASA_SPIM_trm functions
 * to recieve data, and do shift, mask. Finally, transmit the data after those
 * operators.
 *
 * @param[in] mode, SPI transformation general mode.
 * @param[in] ASAID, ASA idendity which user can choose ASA slave board.
 * @param[in] RegAdd, control flag or remote R/W register ID.
 * @param[in] Mask, recieve data do mask after shift them.
 * @param[in] Shift, recieve data do shift.
 * @param[in] *Data_p, data pointer that will transmit.
 * @return char Success or not.
 */
char ASA_SPIM_ftm(char mode, char ASAID, char RegAdd, char Mask, char Shift,
                  char *Data_p) {
    if ((mode == 1) || (mode == 2) || (mode == 3) || (mode == 4) ||
        (mode == 6) || (mode == 8))
        return HAL_ERROR_MODE_SELECT;
    char temp = 0;
    SPIM_Inst.enable_cs(ASAID);
    ASA_SPIM_rec(mode, ASAID, RegAdd, 1, &temp);
    temp = (temp & ~Mask) + ((*Data_p << Shift) & Mask);
    ASA_SPIM_trm(mode, ASAID, RegAdd, 1, &temp);
    SPIM_Inst.disable_cs(ASAID);
    return 0;
}
