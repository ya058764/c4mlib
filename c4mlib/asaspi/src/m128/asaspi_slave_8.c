// #define USE_C4MLIB_DEBUG
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"
void SPIS8_cb(void) {
    uint8_t tempData = 0;
    switch (ASASPISerialIsrStr->sm_status) {
        case SPIS_STATE_IDLE:
            tempData = SPIS_Inst.read_byte();
            // Initialize ASASPISerialIsrStr structure.
            ASASPISerialIsrStr->reg_address = (tempData & 0xFE) >> 1;
            ASASPISerialIsrStr->check_sum = 0;
            SPIS_Inst.error_code = 0;
            ASASPISerialIsrStr->byte_counter =
                ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                    .sz_reg -
                1;
            // Write mode
            if (tempData & 0x01) {
                ASASPISerialIsrStr->sm_status =
                    SPIS_STATE_WRITE;  // master send slave get
                ASASPISerialIsrStr->rw_mode = 0;
            }
            // Read mode
            else {
                ASASPISerialIsrStr->sm_status =
                    SPIS_STATE_READ;  // master get slave send
                ASASPISerialIsrStr->rw_mode = 1;
                SPIS_Inst.write_byte(
                    ASASPISerialIsrStr
                        ->remo_reg[ASASPISerialIsrStr->reg_address]
                        .data_p[ASASPISerialIsrStr->byte_counter]);
                if (ASASPISerialIsrStr->byte_counter > 0) {
                    ASASPISerialIsrStr->byte_counter--;
                }
                else {
                    ASASPISerialIsrStr->sm_status = SPIS_STATE_IDLE;
                }
            }
            break;
        case SPIS_STATE_WRITE:
            tempData = SPIS_Inst.read_byte();
            ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                .data_p[ASASPISerialIsrStr->byte_counter] = tempData;
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            else {
                ASASPISerialIsrStr->sm_status = SPIS_STATE_IDLE;
            }

            break;
        case SPIS_STATE_READ:
            tempData = SPIS_Inst.read_byte();
            SPIS_Inst.write_byte(
                ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                    .data_p[ASASPISerialIsrStr->byte_counter]);
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            else {
                ASASPISerialIsrStr->sm_status = SPIS_STATE_IDLE;
            }
            break;
        default:
            break;
    }
    DEBUG_INFO("tempData:%d, status:%d, byte_counter:%d\n", tempData,
               ASASPISerialIsrStr->sm_status, ASASPISerialIsrStr->byte_counter);
}
