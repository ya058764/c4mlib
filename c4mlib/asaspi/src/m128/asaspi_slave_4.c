// #define USE_C4MLIB_DEBUG
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"
void SPIS4_cb(void) {
    static bool fg = true;
    uint8_t tempData = 0;
    switch (ASASPISerialIsrStr->rw_mode) {
        // write mode, slave read data from master
        case 0:
            if (fg) {
                ASASPISerialIsrStr->byte_counter =
                    ASASPISerialIsrStr->remo_reg[2].sz_reg - 1;
                fg = false;
            }
            tempData = SPIS_Inst.read_byte();
            ASASPISerialIsrStr->remo_reg[2]
                .data_p[ASASPISerialIsrStr->byte_counter] = tempData;
            DEBUG_INFO("tempData:%d, byte_counter:%d\n", tempData,
                       ASASPISerialIsrStr->byte_counter);
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            else {
                fg = true;
            }
            break;
        // read mode, master recieve data from master
        case 1:
            if (fg) {
                ASASPISerialIsrStr->byte_counter =
                    ASASPISerialIsrStr->remo_reg[2].sz_reg - 1;
                fg = false;
            }
            if (ASASPISerialIsrStr->byte_counter == 255) {
                SPIS_Inst.write_byte(0);
                fg = true;
            }
            else {
                SPIS_Inst.write_byte(
                    ASASPISerialIsrStr->remo_reg[2]
                        .data_p[ASASPISerialIsrStr->byte_counter]);
                DEBUG_INFO("send:%d, byte_counter:%d\n",
                           ASASPISerialIsrStr->remo_reg[2]
                               .data_p[ASASPISerialIsrStr->byte_counter],
                           ASASPISerialIsrStr->byte_counter);
            }
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            else if (ASASPISerialIsrStr->byte_counter == 0) {
                ASASPISerialIsrStr->byte_counter = 255;
            }
            break;
        default:
            break;
    }
}