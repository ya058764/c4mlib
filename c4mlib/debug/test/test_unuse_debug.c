/**
 * @file test_debug.c
 * @author LiYu87
 * @date 2019.03.27
 * @brief 測試不使用macro USE_C4MLIB_DEBUG
 *
 * 測試不使用macro USE_C4MLIB_DEBUG時，DEBUG_INFO是否無效果。
 */
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/stdio/src/stdio.h"

int main() {
    ASA_STDIO_init();
    printf("test C4MLIB_DEBUG\n");
    DEBUG_INFO("test\n");

    return 0;
}
