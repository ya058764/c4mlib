##
# @file makefile
# @author LiYu87
# @date 2019.01.28
# @brief 模組建置的makefile

## Usage : 編譯目標 objects
# NOTE : 若更換硬體平台，編譯前請先使用 make clean 及 make clean_deps 清除所有 objects
# 使用 make 指令會編譯出此模組的目標 objects

## Usage : 編譯測試檔案
# NOTE : 若更換硬體平台，編譯前請先使用 make clean 及 make clean_deps 清除所有 objects
# 1. make deps
# 2. make test
#
# make test 指令，會去尋找 test 目錄下的測試檔案，並編譯成 hex 檔案
# make xxx.hex 指令，會自動尋找 test 目錄下的 xxx.c 編譯

## 參數設置
# 預設的編譯環境使用MCU為atega128、F_CPU為11059200
# 若要更改該測試的平台需要手動把參數傳入make中
# Ex : make test_m88.hex -MCU=atmega88 -F_CPU=8000000
# 也可以讓它變成 makefile 中的指令
# test_m88.hex:
# 	@make test_m88.hex -MCU=atmega88 -F_CPU=8000000

## 模組專案設定
# 模組名字 (需與資料夾名稱相同，且全小寫)
MODULE_NAME = time

# 在參數 TARGETS 中加入此模組的目標 objects
TARGETS  = hal_time.o
TARGETS += timeout.o

## 以下參數，請勿隨意更改
# 從 ../modules.mk 取得變數 MODULES
# 並把變數 MODULES 中的 MODULE_NAME 過濾掉，放入變數 DEP_MODULES
include ../modules.mk
DEP_MODULES = $(filter-out $(MODULE_NAME),$(MODULES))

# 預設使用的 make 規則，包含硬體資訊，編譯obj、hex、elf等檔案之規則
GCC_MAKE = ../gcc.mk

# 自動搜尋 src test lib 底下 source
VPATH = src/ test/ lib/

# Include Path
# 預設為專案根目錄
IPATH = ../../

# serch for test
TESTSRCS = $(wildcard test/*.c)
TESTHEXS = $(patsubst %.c,%.hex,$(notdir $(TESTSRCS)))

# serch for unittest
UNITTESTSRCS = $(wildcard unittest/*.c)
UNITTESTHEXS = $(patsubst %.c,%.hex,$(notdir $(UNITTESTSRCS)))

# serch for dependent objs from other modules
FIND_DEP_OBJS = $(wildcard ../$(DIR)/*.o)
DEP_OBJS := $(foreach DIR,$(DEP_MODULES),$(FIND_DEP_OBJS))

# 生成 hex 所需 objects ，包含此模組及相依模組之 objects
LIBOBJS := $(DEP_OBJS) $(TARGETS)

## 預設指令
# 編譯目標檔案
all: $(TARGETS)

# 編譯測試用hex檔案
test: $(TESTHEXS)
	@avr-size $(TESTHEXS)

# 編譯自動測試用hex檔案
unittest: $(UNITTESTHEXS)
	@avr-size $(UNITTESTHEXS)

# 編譯相依模組的 objects
deps:
	$(foreach DIR,$(DEP_MODULES),make -C ../$(DIR) &&) echo 'deps is done!'

%.o: %.c
	@make -f $(GCC_MAKE) $@ VPATH="$(VPATH)" IPATH="$(IPATH)"

%.hex: %.o $(TARGETS)
	@make -f $(GCC_MAKE) $@ LIBOBJS="$(LIBOBJS)" VPATH="$(VPATH)" IPATH="$(IPATH)"

%.lss: %.o $(TARGETS)
	@make -f $(GCC_MAKE) $@ LIBOBJS="$(LIBOBJS)" VPATH="$(VPATH)" IPATH="$(IPATH)"

## 清除此模組內物件
clean:
	-rm -rf $(TARGETS) *.elf *.hex *.map *.o *.lss

## 清除此相依模組內物件
clean_deps:
	$(foreach DIR,$(DEP_MODULES),make clean -s -C ../$(DIR) &&) echo 'clean is done!'

.PHONY: all test unittest deps clean
