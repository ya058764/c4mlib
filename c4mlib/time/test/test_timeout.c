#define F_CPU 11059200UL

#include "c4mlib/device/src/device.h"
#include "c4mlib/time/src/hal_time.h"
#include "c4mlib/time/src/timeout.h"

#include <avr/interrupt.h>

ISR(TIMER3_COMPA_vect) {
    Timeout_tick();
    HAL_tick();
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer();

// Timeout ISR for Test
void timeout_ISR1();
void timeout_ISR2();

// Timeout post flag
volatile uint8_t timeout_flag1 = 0;
volatile uint8_t timeout_flag2 = 0;
uint8_t timeout_ISR_id1;
uint8_t timeout_ISR_id2;

int main() {
    ASA_STDIO_init();
    ASABUS_ID_init();
    init_timer();

    sei();

    /***** Test the ASA Timeout component *****/
    TimerCntStr_init(&TimerCntStr_inst);
    timeout_ISR_id1 =
        Timeout_reg(timeout_ISR1, &timeout_flag1, 500);  // Delay for 500 ms
    timeout_ISR_id2 =
        Timeout_reg(timeout_ISR2, &timeout_flag2, 1000);  // Delay for 2000 ms

    // Enable Timeout Interrupt
    Timeout_ctl(timeout_ISR_id1, 1);
    Timeout_ctl(timeout_ISR_id2, 1);

    for (int i = 1; i <= 20; i++) {
        printf("=== Delay for %d ms ===\n", 100 * i);
        HAL_delay(100UL * (uint32_t)i);
        timeout_ISR1();
        timeout_ISR2();
    }

    Timeout_ctl(timeout_ISR_id1, 0);

    // Timeout2 repeat occure

    while (1)
        ;  // Block here
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]
    DDRE |= 1 << PE3;
    ICR3 = 11058;
    TCCR3A = 0b01000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}

// Timeout ISR for Test
void timeout_ISR1() {
    if (timeout_flag1 == 1) {
        printf("Timeout ISR1 called by Timeout Interrupt! \n");
    }
    else {
        printf("Timeout ISR1() called ! \n");
    }

    // Reset the timeout counter
    // TimerCntStr_inst.timeoutISR_inst[timeout_ISR_id1].counter =
    // TimerCntStr_inst.timeoutISR_inst[timeout_ISR_id1].time_limit;
    // timeout_flag1 = 0;

    Timeout_reset(timeout_ISR_id1);
}

// Timeout ISR for Test
void timeout_ISR2() {
    if (timeout_flag2 == 1) {
        printf("Timeout ISR2 called by Timeout Interrupt! \n");
    }
    else {
        printf("Timeout ISR2() called ! \n");
    }

    // Reset the timeout counter
    TimerCntStr_inst.timeoutISR_inst[timeout_ISR_id2].counter =
        TimerCntStr_inst.timeoutISR_inst[timeout_ISR_id2].time_limit;
    timeout_flag2 = 0;

    Timeout_reset(timeout_ISR_id2);
}
