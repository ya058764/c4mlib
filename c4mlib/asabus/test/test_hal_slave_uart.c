#define F_CPU 11059200UL

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/time/src/hal_time.h"

ISR(TIMER1_COMPA_vect) {
    HAL_tick();
}

// Initialize the TIME1 with CTC mode, interrupt at 1000 Hz
void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR1A  [COM1A1 COM1A0 COM1B1 COM1B0 COM1C1 COM1C0 WGM11 WGM10]
    // TCCR1B  [ICNC1 ICES1 -- WGM13 WGM12 CS12 CS11 CS10]

    ICR1 = 11058;
    TCCR1A = 0b00000000;
    TCCR1B = 0b00011001;
    TCNT1 = 0x00;
    ETIMSK |= 1 << OCIE1A;
}

#define SZ_DATA_ARRAY 100

int main() {
    // ASA_STDIO_init() use UART0, and Atmega88 also have UART0,
    // The configure program is same.
    ASA_STDIO_init();

    HAL_time_init();
    init_timer();

    sei();

    // Test data set for UART communcation
    uint8_t data_array[SZ_DATA_ARRAY];
    for (int i = 0; i < SZ_DATA_ARRAY; i++) {
        data_array[i] = 'a' + i % 25;
    }

    /***** Test the hal uart i/o operation *****/
    printf("======= One byte write test =======\n");
    TypeOfUARTInst UART_Main = UARTS_Inst;
    for (int i = 0; i < SZ_DATA_ARRAY; i++) {
        UART_Main.write_byte(data_array[i]);  // Write data with one byte method
    }

    _delay_ms(100);
    printf("\n======= Mulit-bytes write test =======\n");
    UART_Main.write_multi_bytes(
        data_array, SZ_DATA_ARRAY);  // Wrtie data with multi-bytes method

    _delay_ms(100);
    printf("\n======= One byte read test =======\n");
    UART_Main.read_byte(data_array);  // Read data with one byte method
    printf("\nError code: %d", *UART_Main.error_code);
    for (int i = 0; i < SZ_DATA_ARRAY; i++) {
        printf("data_array[%d] = %02X %c\n", i, data_array[i], data_array[i]);
    }

    _delay_ms(100);
    printf("\n======= Mulit-bytes read test =======\n");
    UART_Main.read_multi_bytes(
        data_array, SZ_DATA_ARRAY);  // Read data with multi-bytes method
    printf("Error code: %d\n", *UART_Main.error_code);
    for (int i = 0; i < SZ_DATA_ARRAY; i++) {
        printf("data_array[%d] = %02X %c\n", i, data_array[i], data_array[i]);
    }

    while (1)
        ;  // Block here
}
