#include "util.h"

/* public functions declaration section ------------------------------------- */
void ASABUS_UART_init(void);
void ASABUS_UART_transmit(char data);
char ASABUS_UART_receive();

/* define section ----------------------------------------------------------- */
#define DEFAULTUARTBAUD 38400

/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */

/* static function contents section ----------------------------------------- */

/* public function contents section ----------------------------------------- */
void ASABUS_UART_init(void) {
    uint16_t baud = F_CPU / 16 / DEFAULTUARTBAUD - 1;

    UBRR1H = (uint8_t)(baud >> 8);
    UBRR1L = (uint8_t)baud;

    UCSR1B |= (1 << RXEN1) | (1 << TXEN1);
    UCSR1C |= (3 << UCSZ10);
}

void ASABUS_UART_transmit(char data) {
    while (!(UCSR1A & (1 << UDRE1)))
        ;
    UDR1 = data;
}

char ASABUS_UART_receive(void) {
    while (!(UCSR1A & (1 << RXC1)))
        ;
    return UDR1;
}
