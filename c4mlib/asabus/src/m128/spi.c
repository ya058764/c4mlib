#include <avr/io.h>

#include "util.h"

/* public functions declaration section ------------------------------------- */
void ASABUS_SPI_init(void);
char ASABUS_SPI_swap(char data);

/* define section ----------------------------------------------------------- */

/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */

/* static function contents section ----------------------------------------- */

/* public function contents section ----------------------------------------- */
void ASABUS_SPI_init(void) {
    // set mosi, sck as outputs
    // set miso as inputs
    REGFPT(BUS_SPI_DDR, BUS_SPI_MASK, BUS_SPI_SHIFT, BUS_SPI_OUT);
    // Enable SPI, set clock rate fosc/64, SPI Transfer format mode 0
    SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR1) | (0 << SPR0);
};

char ASABUS_SPI_swap(char data) {
    SPDR = data;
    while (!(SPSR & (1 << SPIF)))
        ;
    return SPDR;
}
