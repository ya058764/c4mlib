#define F_CPU 11059200UL
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asauart/src/asauart_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/interrupt/src/extint.h"
#include "c4mlib/interrupt/src/intfreqdiv.h"
#include "c4mlib/interrupt/src/timint.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

#include "c4mlib/config/remo_reg.cfg"

void init_ext0();

void USER_FUNCTION(void* pvData);

/* 主程式區塊 */
int main() {
/* 常數巨集定義區段 */
#define REMOREG_0_SIZE (4)
#define REMOREG_0_INI (0)

    /* Variables 變數宣告及定義區段 */
    // 宣告外部中斷硬體管理用資料結構
    TypeOfExtInt ExtInt0 = EXTINT_0_STR_INI;
    // 宣告串列通訊埠管理用資料結構
    TypeOfSerialIsr SerialIsrUart = SERIAL_ISR_STR_UART_INI;
    // 宣告儲存可遠端讀寫暫存器的地址編號變數
    uint8_t remoreg0AddressId;
    // 宣告儲存外部中斷中執行工作的工作編號變數
    uint8_t EXTFB0Id;
    // 宣告可遠端讀寫變數
    uint32_t remoreg0 = REMOREG_0_INI;

    /* 函式庫內建及使用者自建初始設定函式區段 */

    // 呼叫ASA_DEVICE_set()執行ASA函式庫之初始化設定。
    ASA_DEVICE_set();
    // 執行外部中斷初始化設定
    init_ext0();
    // 呼叫ExtInt_init()執行外部中斷硬體管理用資料結構初始化設定
    ExtInt_init(&ExtInt0);
    // 呼叫SerialIsrStr_init()執行串列通訊埠管理用資料結構初始化設定
    // FIXME: 使用新版初始化
    SerialIsr_init(&SerialIsrUart, 0);
    // 呼叫ExtInt_reg()登陸外部中斷中執行工作並取得編號
    ExtInt_reg(&ExtInt0, USER_FUNCTION, &remoreg0);
    // 呼叫RemoRW_reg()登陸可遠端讀寫變數並取得編號
    remoreg0AddressId = RemoRW_reg(&SerialIsrUart, remoreg0, REMOREG_0_SIZE);
    //回傳可遠端讀寫變數登陸結果(發布階段需刪除)
    printf("Create RemoRWreg [%u] with %u bytes \n", remoreg0AddressId,
           SerialIsrUart.remo_reg[remoreg0AddressId].sz_reg);

    // 呼叫sei()開啟全域中斷
    sei();

    while (1)
        ;  // Block here
}

/* 副函式區段 */
// 計時中斷中執行工作副函式區段
void USER_FUNCTION(void* pvData) {
    // 執行使用者定義之指定工作

    // 將結果放入先前宣告之可遠端讀寫變數
}

void init_ext0() {
    // TODO: 完成設定EXT0
}
