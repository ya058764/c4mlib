#define F_CPU 11059200UL
#include "c4mlib/device/src/device.h"
#include "c4mlib/interrupt/src/extint.h"
#include "c4mlib/interrupt/src/intfreqdiv.h"
#include "c4mlib/interrupt/src/timint.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

// TODO: Initial macro Change to another file
#include "c4mlib/config/interrupt.cfg"

TypeOfIntFreqDiv IntFreqDivStr_1;
// TypeOfIntFreqDiv IntFreqDivStr_2;

TypeOfTimInt TimInt3 = TIMINT_3_STR_INI;

// TypeOfExtInt ExtInt0 = EXTINT_0_STR_INI;

uint8_t id_task1_timeout = 0;
uint8_t id_task2_timeout = 0;
uint8_t id_task3_timeout = 0;
uint8_t id_task4_timeout = 0;

void init_timer();

void USER_FUNCTION1(void* pvData);
void USER_FUNCTION2(void* pvData);

int main() {
    ASA_DEVICE_set();

    /***** Test the  IntFreqDiv component *****/
    printf("======= Test IntFreqDiv component =======\n");

    printf("Setup PORTA[0:7] output\n");
    DDRA = 0xFF;

    printf("Start timer3 with CTC 1000Hz\n");
    init_timer();
    printf("Enable Global Interrupt\n");
    sei();

    uint8_t param_0 = 0;
    uint8_t param_20 = 20;
    uint8_t param_40 = 40;
    uint8_t param_60 = 60;
    uint8_t param_80 = 80;

    //             ISRStr_p          ISRFunc_p      ISRFuncStr_p cycle phase
    id_task1_timeout =
        IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION1, &param_20, 0, 10);
    printf("Added 0 cycle,10 phase to IntFreqDiv 1 Component\n");
    id_task2_timeout =
        IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION1, &param_40, 0, 10);
    printf("Added 0 cycle,10 phase to IntFreqDiv 1 Component\n");
    id_task3_timeout =
        IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION1, &param_60, 0, 10);
    printf("Added 0 cycle,10 phase to IntFreqDiv 1 Component\n");
    id_task4_timeout =
        IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION1, &param_80, 0, 10);
    printf("Added 0 cycle,10 phase to IntFreqDiv 1 Component\n");

    uint8_t id_task1 =
        IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION2, &param_20, 100, 20);
    printf("Added 100 cycle,20 phase to IntFreqDiv 1 Component\n");
    uint8_t id_task2 =
        IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION2, &param_40, 100, 40);
    printf("Added 100 cycle,40 phase to IntFreqDiv 1 Component\n");
    uint8_t id_task3 =
        IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION2, &param_60, 100, 60);
    printf("Added 100 cycle,60 phase to IntFreqDiv 1 Component\n");
    uint8_t id_task4 =
        IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION2, &param_80, 100, 80);
    printf("Added 100 cycle,70 phase to IntFreqDiv 1 Component\n");

    IntFreqDiv_en(&IntFreqDivStr_1, id_task1, 1);
    IntFreqDiv_en(&IntFreqDivStr_1, id_task2, 1);
    IntFreqDiv_en(&IntFreqDivStr_1, id_task3, 1);
    IntFreqDiv_en(&IntFreqDivStr_1, id_task4, 1);

    printf("Initize the TimInt component with TimInt3\n");
    // Connect to Timer
    TimInt_init(&TimInt3);

    printf("Added IntFreqDiv 1 to TimInt3\n");
    //          ISRStr_p  ISRFunc_p       ISRFuncParam_p
    uint8_t id_timer3 = TimInt_reg(&TimInt3, IntFreqDiv_step, &IntFreqDivStr_1);

    TimInt_en(&TimInt3, id_timer3, 1);

    while (1)
        ;  // Block here
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11059;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}

void USER_FUNCTION1(void* pvData) {
    uint8_t* p_param = (uint8_t*)pvData;

    if (p_param != NULL) {
        switch (*p_param) {
            case 20:
                PORTA &= ~1;
                break;
            case 40:
                PORTA &= ~2;
                break;
            case 60:
                PORTA &= ~4;
                break;
            case 80:
                PORTA &= ~8;
                break;
        }
    }
}

void USER_FUNCTION2(void* pvData) {
    uint8_t* p_param = (uint8_t*)pvData;

    if (p_param != NULL) {
        switch (*p_param) {
            case 20:
                PORTA |= 1;
                IntFreqDiv_en(&IntFreqDivStr_1, id_task1_timeout, 1);
                break;
            case 40:
                PORTA |= 2;
                IntFreqDiv_en(&IntFreqDivStr_1, id_task2_timeout, 1);
                break;
            case 60:
                PORTA |= 4;
                IntFreqDiv_en(&IntFreqDivStr_1, id_task3_timeout, 1);
                break;
            case 80:
                PORTA |= 8;
                IntFreqDiv_en(&IntFreqDivStr_1, id_task4_timeout, 1);
                break;
        }
    }
}
