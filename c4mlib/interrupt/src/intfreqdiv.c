// TODO: 完成此份文件註解

#include "intfreqdiv.h"

#include <stddef.h>

void IntFreqDiv_init(TypeOfIntFreqDiv* IFDStr_p) {
    // TODO: Add macro initialize code
}

// Change the Enable/Disable configure for specific task object
void IntFreqDiv_en(TypeOfIntFreqDiv* IFDStr_p, uint8_t isr_id, uint8_t enable) {
    // TODO: Check the parameter is valid
    // Makesure the is not nullptr
    if (IFDStr_p != NULL) {
        IFDStr_p->Task[isr_id].enable = enable;
    }
}

void IntFreqDiv_step(TypeOfIntFreqDiv* IFDStr_p) {
    // 依序將已註冊的所以中斷除頻工作進行Counter更新，並完成觸發呼叫
    for (int i = 0; i < IFDStr_p->total; i++) {
        // Only check enable item
        if (!IFDStr_p->Task[i].enable) {
            // If there is disable, skip
            continue;
        }

        // 判斷是否為One-shot mode
        if (IFDStr_p->Task[i].cycle == 0) {
            // 判斷是否倒數完畢
            if (IFDStr_p->Task[i].counter == 0) {
                // 不自動 Reload動作，單次觸發對應工作
                // TODO: 增加抽象化呼叫方式，相容Lutos排程呼叫。
                IFDStr_p->Task[i].postSlot = 1;
                IFDStr_p->Task[i].func_p(IFDStr_p->Task[i].funcPara_p);
                IFDStr_p->Task[i].postSlot = 0;

                // 禁能該中斷工作，以達成單次觸發功能。如要再觸發需重新致能
                IntFreqDiv_en(IFDStr_p, i, 0);
                // 重置計數器
                IFDStr_p->Task[i].counter = IFDStr_p->Task[i].phase;
            }
        }
        // Auto-reload mode
        else {
            // 判斷是否倒數完畢
            if (IFDStr_p->Task[i].counter == 0) {
                // 執行Reload動作
                IFDStr_p->Task[i].counter = IFDStr_p->Task[i].cycle;
            }
            // 判斷是否符合相位，phase的基準點為 (counter%cycle)==0的時刻
            if (IFDStr_p->Task[i].cycle - IFDStr_p->Task[i].counter ==
                IFDStr_p->Task[i].phase) {
                // 執行對應工作
                IFDStr_p->Task[i].postSlot = 1;
                IFDStr_p->Task[i].func_p(IFDStr_p->Task[i].funcPara_p);
                IFDStr_p->Task[i].postSlot = 0;
            }
        }
        IFDStr_p->Task[i].counter--;
    }
}

// Register IntFreqDiv instance
uint8_t IntFreqDiv_reg(TypeOfIntFreqDiv* IFDStr_p, Func_t func_p,
                       void* funcPara_p, uint16_t cycle, uint16_t phase) {
    uint8_t new_IFDStrId = IFDStr_p->total;
    IFDStr_p->Task[new_IFDStrId].enable = 0;  // Default Disable Timeout ISR
    // 判斷是否為One-shot模式
    if (cycle == 0) {
        IFDStr_p->Task[new_IFDStrId].counter = phase;
    }
    else {
        IFDStr_p->Task[new_IFDStrId].counter = cycle;
    }

    IFDStr_p->Task[new_IFDStrId].cycle = cycle;
    IFDStr_p->Task[new_IFDStrId].phase = phase;
    IFDStr_p->Task[new_IFDStrId].func_p = func_p;
    IFDStr_p->Task[new_IFDStrId].funcPara_p = funcPara_p;
    IFDStr_p->Task[new_IFDStrId].postSlot = 0;
    IFDStr_p->total++;

    return new_IFDStrId;
}
