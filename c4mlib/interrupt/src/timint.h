/**
 * @file timint.h
 * @author Ye cheng-Wei
 * @author LiYu87
 * @date 2019.04.01
 * @brief
 * 提供ASA函式庫標準計時器中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */

// TODO: 完成此份文件註解

#ifndef C4MLIB_ASA_TIMINT_H
#define C4MLIB_ASA_TIMINT_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_type.h"

#include <stddef.h>
#include <stdint.h>

// Include Configure file
#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct {
    const uint8_t TIM_UID;  // 紀錄本結構歸屬UID，用於init()函式判斷鏈接目標
    uint8_t total;  // 紀錄已註冊多少計時中斷
    volatile TypeOfISRFunc task[MAX_TIM_FuncNum];  // 紀錄所有已註冊的計時中斷
} TypeOfTimInt;

// Initialize the TypeOfTimInt struct
void TimInt_init(TypeOfTimInt* timInt_p);

// Register TIM Isr instance
uint8_t TimInt_reg(TypeOfTimInt* timInt_p, Func_t isrFunc_p,
                   void* isrFuncPara_p);

// Change the Enable/Disable configure for specific Isr task object
void TimInt_en(TypeOfTimInt* timInt_p, uint8_t isr_id, uint8_t enable);
/* Public Section End */

void TimInt_step(TypeOfTimInt* timInt_p);

/* Library memory mapping table Start */
TypeOfTimInt* RedirectTableOfTimIntStr[MAX_ISR_TIMER_NUM];
/* Library memory mapping table End */

#endif  // C4MLIB_ASA_TIMINT_H
