/**
 * @file timint.c
 * @author Ye cheng-Wei
 * @author LiYu87
 * @date 2019.04.01
 * @brief
 * 提供ASA函式庫標準計時器中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */

// TODO: 完成此份文件之註解

/**
 * NOTE
 * TimInt_step is used by hardware interrupt
 * and its code is in hardware/std_isr.
 */

#include "timint.h"

/* Library memory mapping table Start */
TypeOfTimInt* RedirectTableOfTimIntStr[MAX_ISR_TIMER_NUM] = {0};
/* Library memory mapping table End */

// Initialize the TypeOfTimInt struct and linking to Library's TIM_isr()
void TimInt_init(TypeOfTimInt* timInt_p) {
    // Makesure the pointer is valid
    if (timInt_p != NULL) {
        RedirectTableOfTimIntStr[timInt_p->TIM_UID] = timInt_p;
    }
}

// Register TIM Isr instance
uint8_t TimInt_reg(TypeOfTimInt* timInt_p, Func_t isrFunc_p,
                   void* isrFuncPara_p) {
    // Makesure the pointer is valid
    if (timInt_p == NULL) {
        return -1;
    }

    uint8_t new_Id = timInt_p->total;
    timInt_p->task[new_Id].enable = 0;  // Default Disable
    timInt_p->task[new_Id].func_p = isrFunc_p;
    timInt_p->task[new_Id].funcPara_p = isrFuncPara_p;
    timInt_p->task[new_Id].postSlot = 0;
    timInt_p->total++;

    return new_Id;
}

// Change the Enable/Disable configure for specific Isr task object
void TimInt_en(TypeOfTimInt* timInt_p, uint8_t isr_Id, uint8_t enable) {
    // Makesure the pointer is valid
    if (timInt_p == NULL) {
        return;
    }
    timInt_p->task[isr_Id].enable = enable;

    return;
}

// Called by internal ISR, private function
void TimInt_step(TypeOfTimInt* timInt_p) {
    // Call Each Enable task
    for (uint8_t i = 0; i < timInt_p->total; i++) {
        if (timInt_p->task[i].enable) {
            // TODO: V4.0.0 targert: Modify to Lutos compatiable execute
            // methold. NOTE: postSlot is On at func_p execute routine!
            timInt_p->task[i].postSlot = 1;
            timInt_p->task[i].func_p(timInt_p->task[i].funcPara_p);
            timInt_p->task[i].postSlot = 0;
        }
    }
}
