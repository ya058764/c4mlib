/**
 * @file std_res.h
 * @author LiYu87
 * @date 2019.03.25
 * @brief Provide ASA standard function response number.
 */

#ifndef C4MLIB_MACRO_STD_TYPE_H
#define C4MLIB_MACRO_STD_TYPE_H

/* Public Section Start */
typedef void (*Func_t)(void *);
/* Public Section End */

#include <stddef.h>
#include <stdint.h>

#endif  // C4MLIB_MACRO_STD_TYPE_H
