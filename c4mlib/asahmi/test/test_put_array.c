/**
 * @file test_put_array.c
 * @author LiYu87
 * @date 2019.07.02
 * @brief 測試函式 HMI_put_array
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

int main() {
    ASA_STDIO_init();

    float data[5] = {1.1, -1, 0, 1, -2.1};
    char s[20];
    char num = 5;

    printf("send f32*5 \n");               // send msg out
    HMI_put_array(HMI_TYPE_F32, 5, data);  // send data

    return 0;
}
