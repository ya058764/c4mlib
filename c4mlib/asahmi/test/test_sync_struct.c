/**
 * @file test_sync_struct.c
 * @author LiYu87
 * @date 2019.07.02
 * @brief
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

typedef struct St {
    uint8_t ui8[10];
    int32_t i32[10];
    float f32[5];
} st_t;

int main() {
    ASA_STDIO_init();

    st_t data = {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                 {2, 4, 8, 16, 32, 64, 128, 256, 512, 1024},
                 {1.0, 2.5, 4.0, 5.5, 6.0}};
    int bytes = sizeof(st_t);

    HMI_snput_struct("ui8_10,i32_10,f32_5", bytes, &data);
    HMI_snget_struct("ui8_10,i32_10,f32_5", bytes, &data);
    HMI_snput_struct("ui8_10,i32_10,f32_5", bytes, &data);

    return 0;
}
