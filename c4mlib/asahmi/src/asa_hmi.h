/**
 * @file asa_hmi.h
 * @author LiYu87
 * @date 2019.07.02
 * @brief 放置HMI函式原型及Macro
 */

#ifndef C4MLIB_HMI_ASA_HMI_H
#define C4MLIB_HMI_ASA_HMI_H

/**
 * @defgroup hmi HMI函式
 */

/**
 * @defgroup hmi_type_macro HMI資料型態編號macro
 */

/* Public Section Start */
// HMI macro
#define HMI_TYPE_I8 \
    0  ///< 資料型態編號 0，int8_t、char                @ingroup hmi_type_macro
#define HMI_TYPE_I16 \
    1  ///< 資料型態編號 1，int16_t、int                @ingroup hmi_type_macro
#define HMI_TYPE_I32 \
    2  ///< 資料型態編號 2，int32_t、long int           @ingroup hmi_type_macro
#define HMI_TYPE_I64 \
    3  ///< 資料型態編號 3，int64_t                     @ingroup hmi_type_macro
#define HMI_TYPE_UI8 \
    4  ///< 資料型態編號 4，uint8_t、unsigned char      @ingroup hmi_type_macro
#define HMI_TYPE_UI16 \
    5  ///< 資料型態編號 5，uint16_t、unsigned int      @ingroup hmi_type_macro
#define HMI_TYPE_UI32 \
    6  ///< 資料型態編號 6，uint32_t、unsigned long int @ingroup hmi_type_macro
#define HMI_TYPE_UI64 \
    7  ///< 資料型態編號 7，uint64_t                    @ingroup hmi_type_macro
#define HMI_TYPE_F32 \
    8  ///< 資料型態編號 8，float、double               @ingroup hmi_type_macro
#define HMI_TYPE_F64 \
    9  ///< 資料型態編號 9，AVR不支援64位元浮點數       @ingroup hmi_type_macro

// HMI declaration
char HMI_put_array(char Type, char Num, void *Data_p);
char HMI_get_array(char Type, char Num, void *Data_p);

char HMI_put_matrix(char Type, char Dim1, char Dim2, void *Data_p);
char HMI_get_matrix(char Type, char Dim1, char Dim2, void *Data_p);

char HMI_put_struct(char *FormatString, int Bytes, void *Data_p);
char HMI_get_struct(char *FormatString, int Bytes, void *Data_p);

char HMI_snput_array(char Type, char Num, void *Data_p);
char HMI_snget_array(char Type, char Num, void *Data_p);

char HMI_snput_matrix(char Type, char Dim1, char Dim2, void *Data_p);
char HMI_snget_matrix(char Type, char Dim1, char Dim2, void *Data_p);

char HMI_snput_struct(char *FormatString, int Bytes, void *Data_p);
char HMI_snget_struct(char *FormatString, int Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HMI_ASA_HMI_H
