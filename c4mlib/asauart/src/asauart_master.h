/**
 * @file asauart_master.h
 * @author Ye cheng-Wei
 * @date 2019.1.22
 * @brief Implement ASA UART Master ModeN function
 *
 *
 */
#ifndef C4MLIB_ASAUART_MASTER_UART_H
#define C4MLIB_ASAUART_MASTER_UART_H

#define CMD_HEADER 0xAA
#define RSP_HEADER 0xAB

/** Public Section Start
 * @brief ASA UART送訊函式
 *
 * @param Mode      UART模式:    目前支援0, 1, 2, 3
 * @param UartID    UART僕ID:    目標裝置的UART ID
 * @param RegAdd    暫存器地址:   要送往的暫存器地址
 * @param Bytes     位元組數:     待送位元組數
 * @param Data_p    待送資料:     待送資料指標
 * @return 通訊結果編號
 */
char UARTM_trm(char Mode, char UartID, char RegAdd, char Bytes, void *Data_p);

/**
 * @brief ASA UART讀取函式
 *
 * @param Mode      UART模式:    目前支援0, 1, 2, 3
 * @param UartID    UART僕ID:    目標裝置的UART ID
 * @param RegAdd    暫存器地址:   要讀取的暫存器地址
 * @param Bytes     位元組數:     待讀的位元組數
 * @param Data_p    待讀資料:     待讀資料的地址指標
 * @return 通訊結果編號
 */
char UARTM_rec(char Mode, char UartID, char RegAdd, char Bytes, void *Data_p);

/**
 * @brief ASA送旗標函式
 *
 * @param Mode      UART模式:       目前支援0, 1, 2, 3
 * @param UartID    UART僕ID:       目標的裝置UART ID
 * @param RegAdd    暫存器地址:      待寫入的暫存器地址
 * @param Mask      遮罩:           待送旗標用遮罩
 * @param shift     左位移數:       待送旗標要左位移的數量
 * @param Data_p    待送旗標資料:   待送旗標資料地址指標
 * @return 通訊結果編號
 */
char UARTM_ftm(char Mode, char UartID, char RegAdd, char Mask, char shift,
               char *Data_p);

/**
 * @brief ASA讀旗標函式
 *
 * @param Mode      UART模式:       目前支援0, 1, 2, 3
 * @param UartID    UART僕ID:       目標的裝置UART ID
 * @param RegAdd    暫存器地址:      待讀取的暫存器地址
 * @param Mask      遮罩:           待讀旗標用遮罩
 * @param shift     左位移數:       待讀旗標要左位移的數量
 * @param Data_p    待送旗標資料:   待讀旗標資料地址指標
 * @return 通訊結果編號
 */
char UARTM_frc(char Mode, char UartID, char RegAdd, char Mask, char shift,
               char *Data_p);
/* Public Section End */

#endif  // C4MLIB_ASAUART_MASTER_UART_H
