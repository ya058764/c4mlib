#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/time/src/timeout.h"

#include "asauart_slave.h"

static uint8_t timeout_IsrID;
static uint8_t timeout_flag = 0;

void UARTS_rx8() {
    // If SerialIsr_init is not call first, There will return
    if (ASAUARTSerialIsrStr == NULL)
        return;

    DEBUG_INFO("UARTS_rx8 call [Timeout:%u]\n", timeout_flag);
    uint8_t data_In;

    if (ASAUARTSerialIsrStr->sm_status == UARTS_SM_HEADER) {
        ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
    }

    switch (ASAUARTSerialIsrStr->sm_status) {
        case UARTS_SM_ADDR:
            /*
                • TOCount=TOut;
                • 讀取UART收值，分解取bit0為RW，bit7:1為暫存器編號。
                • 檢查RW值決定切換狀態。(參考編號3 或4)
            */

            // 開啟逾時中斷
            Timeout_ctl(timeout_IsrID, 1);

            if (timeout_flag) {  // Timeout ISR，回ADDR
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
                ASAUARTSerialIsrStr->result_message = HAL_ERROR_TIMEOUT;

                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                break;
            }

            UARTS_Inst.read_byte(&data_In);
            DEBUG_INFO("[UARTS_SM_ADDR]\tread <%02X>\n", data_In);

            ASAUARTSerialIsrStr->reg_address = (data_In & 0x7f) >> 1;
            ASAUARTSerialIsrStr->rw_mode = data_In & 0x01;

            ASAUARTSerialIsrStr->byte_counter =
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg;
            if (ASAUARTSerialIsrStr->rw_mode ==
                0) {  // UART MODE8寫入模式   // 檢查RW值決定切換狀態 (參考編號3
                      // 或4)
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_DATA;
            }
            else if (ASAUARTSerialIsrStr->rw_mode ==
                     1) {  // UART MODE8 讀取模式
                // Read mode
                // TODO: Report the data, change the method to UARTS_tx() state
                // mechine method via interrupt Record the data into the
                // temproary buffer

                DEBUG_INFO("Process read mode, UARTS_tx8 ccall \n");
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_BYTES;
                UARTS_tx8();
            }

            break;

        case UARTS_SM_DATA:
            /*
                • TOCount=TOut;
                • 讀取UART收值，轉存入BUFF(ByteCount  )中。
                • ByteCount =ByteCount -1
                • TotalBytes= RemoRW_reg表第暫存器編號個暫存器Byte數
                • 檢查ByteCount 值決定切換狀態。
            */
            if (timeout_flag) {  // Timeout ISR，回ADDR
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
                ASAUARTSerialIsrStr->result_message = HAL_ERROR_TIMEOUT;

                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                break;
            }

            UARTS_Inst.read_byte(&data_In);
            DEBUG_INFO("[UARTS_SM_DATA]\tread <%02X>\n", data_In);
            DEBUG_INFO(
                "byte_counter: <%u>, sz_reg: <%u>\n",
                ASAUARTSerialIsrStr->byte_counter,
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg);
            ASAUARTSerialIsrStr->temp[ASAUARTSerialIsrStr->byte_counter - 1] =
                data_In;
            ASAUARTSerialIsrStr->byte_counter--;

            if (ASAUARTSerialIsrStr->byte_counter == 0) {
                DEBUG_INFO("Process write mode\n");
                DEBUG_INFO("ASAUARTSerialIsrStr addrewss:%x\n",
                           ASAUARTSerialIsrStr);
                // Move temporary data in buffer to target register memory
                for (int i = 0;
                     i < ASAUARTSerialIsrStr
                             ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                             .sz_reg;
                     i++) {
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .data_p[i] = ASAUARTSerialIsrStr->temp[i];
                }
                // Process Modify event Callback
                if (ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .func_p != NULL)
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .func_p(ASAUARTSerialIsrStr
                                    ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                                    .funcPara_p);

                DEBUG_INFO("Process write register Done\n");

                ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
                ASAUARTSerialIsrStr->byte_counter =
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .sz_reg -
                    1;

                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
            }

            break;
    }

    // Reset the timeout ISR
    TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].counter =
        TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].time_limit;
    timeout_flag = 0;
}

void UARTS_tx8() {
    switch (ASAUARTSerialIsrStr->sm_status) {
        case UARTS_SM_BYTES:

            /**** Write the Data to Master ****/

            for (int i = (ASAUARTSerialIsrStr->byte_counter - 1); i >= 0; i--) {
                UARTS_Inst.write_byte(
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .data_p[i]);
            }

            ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
            ASAUARTSerialIsrStr->byte_counter =
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg -
                1;

            // 關閉逾時中斷
            Timeout_ctl(timeout_IsrID, 0);

            break;
    }
}
