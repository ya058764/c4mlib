#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/time/src/timeout.h"

#include "asauart_slave.h"

static uint8_t timeout_IsrID;
static uint8_t timeout_flag = 0;

// TODO UART MODE3 確認規格
// ! 注意：在rx3() 裡面 使用printf() ，會造成資料掉包。

void UARTS_rx3() {
    if (ASAUARTSerialIsrStr == NULL)
        return;  // If SerialIsrStr_init is not call first, There will return
    ASAUARTSerialIsrStr->reg_address = 2;
    DEBUG_INFO("UARTS_rx3 call [Timeout:%u]\n", timeout_flag);
    uint8_t data_In;
    if (ASAUARTSerialIsrStr->sm_status == UARTS_SM_HEADER) {
        ASAUARTSerialIsrStr->sm_status =
            UARTS_SM_DATA;  //因狀態機沒Header狀態，所以直接引至Data狀態
    }

    switch (ASAUARTSerialIsrStr->sm_status) {
        case UARTS_SM_DATA:
            if (timeout_flag) {  // 逾時發生
                ASAUARTSerialIsrStr->result_message =
                    HAL_ERROR_TIMEOUT;  // 回傳Timeout error 錯誤訊息
                Timeout_ctl(timeout_IsrID, 0);  // 關閉逾時中斷
                break;
            }

            UARTS_Inst.read_byte(&data_In);  // 讀取 1 byte 資料
            DEBUG_INFO("[UARTS_SM_DATA]\tread <%02X>\n", data_In);
            DEBUG_INFO(
                "byte_counter: <%u>, sz_reg: <%u>\n",
                ASAUARTSerialIsrStr->byte_counter,
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg);

            // 將讀取到的  1 byte 資料 存入temp[]，由低到高
            ASAUARTSerialIsrStr->temp[ASAUARTSerialIsrStr->byte_counter] =
                data_In;
            ASAUARTSerialIsrStr->byte_counter++;

            // 當 byte_counter 與 全 sz_reg相同時，byte_counter 重置、歸0
            if (ASAUARTSerialIsrStr->byte_counter ==
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg) {
                ASAUARTSerialIsrStr->byte_counter = 0;

                DEBUG_INFO("Process write mode\n");
                DEBUG_INFO("ASAUARTSerialIsrStr address:%x\n",
                           ASAUARTSerialIsrStr);
                // Move temporary data in buffer to target register memory
                for (int i = 0;
                     i < ASAUARTSerialIsrStr
                             ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                             .sz_reg;
                     i++) {
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .data_p[i] = ASAUARTSerialIsrStr->temp[i];
                }
                // Process Modify event Callback
                if (ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .func_p != NULL)
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .func_p(ASAUARTSerialIsrStr
                                    ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                                    .funcPara_p);

                DEBUG_INFO("Process write register Done\n");

                ASAUARTSerialIsrStr->byte_counter = 0;
                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                /* ---------------- 呼叫 Tx 傳送資料 -------------------*/
                UARTS_tx3();
            }

            break;
    }

    // Reset the timeout ISR
    TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].counter =
        TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].time_limit;
    timeout_flag = 0;  // timeout 旗標放下
}
void UARTS_tx3() {
    DEBUG_INFO("UARTS_tx3 call [Timeout:%u]\n", timeout_flag);
    //_delay_ms(28);    // 讓tx delay，不然太快會使rec漏接資料 (有printf)
    _delay_ms(45);  // 讓tx delay，不然太快會使rec漏接資料 (沒有printf)
    // printf("UARTS_tx3 call [Timeout:%u]\n",timeout_flag);
    ASAUARTSerialIsrStr->reg_address = 3;
    if (timeout_flag) {  // 逾時發生
        ASAUARTSerialIsrStr->result_message =
            HAL_ERROR_TIMEOUT;          // 回傳Timeout error 錯誤訊息
        Timeout_ctl(timeout_IsrID, 0);  // 關閉逾時中斷
    }
    /**** Write the Data to Master ****/
    if (ASAUARTSerialIsrStr->byte_counter ==
        ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
            .sz_reg) {
        ASAUARTSerialIsrStr->byte_counter = 0;
        ASAUARTSerialIsrStr->rw_mode = 0;
        Timeout_ctl(timeout_IsrID, 0);  // 關閉逾時中斷
    }
    else {
        // printf("byte_counter=%d
        // ,data=%d\n",ASAUARTSerialIsrStr->byte_counter,ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address].data_p[ASAUARTSerialIsrStr->byte_counter]);
        DEBUG_INFO("data=%u\n", ASAUARTSerialIsrStr
                                    ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                                    .data_p[ASAUARTSerialIsrStr->byte_counter]);
        UARTS_Inst.write_byte(
            ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                .data_p[ASAUARTSerialIsrStr->byte_counter]);
        ASAUARTSerialIsrStr->byte_counter++;
    }
}
