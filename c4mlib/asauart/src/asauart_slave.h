/**
 * @file asauart_slave.h
 * @author Ye cheng-Wei
 * @date 2019.1.22
 * @brief Implement ASA Slave UART Mode0
 *
 *
 */

#ifndef C4MLIB_ASAUART_ASAUART_SLAVE_H
#define C4MLIB_ASAUART_ASAUART_SLAVE_H

#define UART_BROADCAST_ID 0

#define DEFAULTUARTBAUD 38400

#define CMD_HEADER 0xAA
#define RSP_HEADER 0xAB
#define DATAOK 0x00

#define UARTS_SM_HEADER 0 /**< UART解包器狀態機代號:  等待標頭 */
#define UARTS_SM_UID 1    /**< UART解包器狀態機代號:  等待DEVICE ID */
#define UARTS_SM_ADDR 2 /**< UART解包器狀態機代號:  等待Register Address */
#define UARTS_SM_BYTES 3 /**< UART解包器狀態機代號:  等待位元組數 */
#define UARTS_SM_DATA 4   /**< UART解包器狀態機代號:  等待資料 */
#define UARTS_SM_CHKSUM 5 /**< UART解包器狀態機代號:  等待檢查碼 */

/**
 * @brief 串列埠中斷執行片段
 *
 * 進行UART封包解包，解包狀態機轉移與執行皆於呼叫階段執行
 *
 * @warning 必須先將UART
 * Interrupt致能，並注意是否已於RemoReg_init()內實作將此函式連接至uart_hal.c內的UARTS_inst.rx_compelete_cb
 */
void UARTS_rx0(void);
void UARTS_rx1(void);
void UARTS_rx2(void);
void UARTS_rx3(void);
void UARTS_rx4(void);
void UARTS_rx5(void);
void UARTS_rx6(void);
void UARTS_rx7(void);
void UARTS_rx8(void);

void UARTS_tx0(void);
void UARTS_tx1(void);
void UARTS_tx2(void);
void UARTS_tx3(void);
void UARTS_tx4(void);
void UARTS_tx5(void);
void UARTS_tx6(void);
void UARTS_tx7(void);
void UARTS_tx8(void);

#endif  // C4MLIB_ASAUART_ASAUART_SLAVE_H
