#define F_CPU 11059200UL

#include "c4mlib/asauart/src/asauart_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/time/src/hal_time.h"

#include <util/delay.h>

#define test_mode 4

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer();

void init_uart1();

int main() {
    ASA_DEVICE_set();
    HAL_time_init();

    init_timer();
    UARTM_Inst.init();

    sei();

    // Broadcast ID
    uint8_t uid = 127;

    uint8_t data_buffer[10] = {5, 6, 7};
    uint8_t result = 0;

    /***** UARTM Mode0 Transmit test *****/
    result = UARTM_trm(test_mode, uid, 2, 1, data_buffer);
    if (result)
        printf("<1>UARTM_trm Fail [%d]\n", result);

    result = UARTM_trm(test_mode, uid, 3, 2, data_buffer);
    if (result)
        printf("<2>UARTM_trm Fail [%d]\n", result);

    result = UARTM_trm(test_mode, uid, 4, 3, data_buffer);
    if (result)
        printf("<3>UARTM_trm Fail [%d]\n", result);

    HAL_delay(50);
    /***** UARTM Mode0 Received test *****/
    result = UARTM_rec(test_mode, uid, 5, 1, data_buffer);
    if (result)
        printf("<1>UARTM_rec Fail [%d]\n", result);

    result = UARTM_rec(test_mode, uid, 6, 10, data_buffer);
    if (result)
        printf("<2>UARTM_rec Fail [%d]\n", result);

    printf("<1>UARTM_rec : %u\n", data_buffer[0]);
    for (int i = 0; i < 10; i++) {
        printf("<2>UARTM_rec : data[%u]=%u\n", i, data_buffer[i]);
    }

    /***** UARTM Mode2 Transmit test *****/
    // TODO: Add Test code

    /***** UARTM MODE2 Transmit test *****/
    // TODO: Add Test code

    /***** UARTM Mode3 Transmit test *****/
    // TODO: Add Test code

    /***** UARTM MODE3 Transmit test *****/
    // TODO: Add Test code

    while (1) {
    }  // Block here
    return 0;
}

void init_uart1() {
    // TODO:
    // Initialize the UART0 with 38400 Baudrate
    uint16_t baud = F_CPU / 16 / 38400 - 1;
    UBRR1H = (unsigned char)(baud >> 8);
    UBRR1L = (unsigned char)baud;

    UCSR1B |= (1 << RXEN1) | (1 << TXEN1);
    UCSR1C |= (3 << UCSZ10);

    // Most enable UCSR
}

void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
