#define F_CPU 11059200UL

#include "c4mlib/asauart/src/asauart_master.h"
#include "c4mlib/common/src/common.h"
#include "c4mlib/common/src/hal_time.h"
#include "c4mlib/hardware/src/isr.h"

#include <util/delay.h>

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer();

int main() {
    ASA_DEVICE_set();
    HAL_time_init();

    init_timer();
    UARTM_Inst.init();

    sei();

    uint8_t data_buffer[5] = {5, 6, 7};
    uint8_t result = 0;

    /***** UARTM Mode8 Transmit test *****/

    printf(" Master transmit 1 bytes data to Slave \n");
    result = UARTM_trm(8, 0, 2, 1, data_buffer);
    if (result) {
        printf("【Test 1】UARTM_trm Fail [%d]\n", result);
    }
    for (int i = 0; i < 5; i++) {
        printf("【Test 1】 UARTM_trm : data[%u]=%u\n", i, data_buffer[i]);
    }

    printf(" Master transmit 3 bytes data to Slave \n");
    result = UARTM_trm(8, 0, 3, 3, data_buffer);
    if (result) {
        printf("【Test 2】UARTM_trm Fail [%d]\n", result);
    }
    for (int i = 0; i < 5; i++) {
        printf("【Test 2】 UARTM_trm : data[%u]=%u\n", i, data_buffer[i]);
    }

    /***** UARTM Mode8 Received test *****/

    printf(" Master receive 1 bytes data from Slave \n");
    result = UARTM_rec(8, 0, 4, 1, data_buffer);
    if (result) {
        printf("【Test 3】UARTM_rec Fail [%d]\n", result);
    }
    for (int i = 0; i < 5; i++) {
        printf("【Test 3】UARTM_rec : data[%u]=%u\n", i, data_buffer[i]);
    }

    printf(" Master receive 5 bytes data from Slave \n");
    result = UARTM_rec(8, 0, 5, 5, data_buffer);
    if (result) {
        printf("【Test 4】UARTM_rec Fail [%d]\n", result);
    }
    for (int i = 0; i < 5; i++) {
        printf("【Test 4】UARTM_rec : data[%u]=%u\n", i, data_buffer[i]);
    }

    while (1) {
    }  // Block here
}

void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}
