#define F_CPU 11059200UL

#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asauart/src/asauart_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_uart.h"
#include "c4mlib/hardware/src/isr.h"
#include "c4mlib/time/src/hal_time.h"
#include "c4mlib/time/src/timeout.h"

#include "c4mlib/config/remo_reg.cfg"

ISR(TIMER1_COMPA_vect) {
    Timeout_tick();
    HAL_tick();
}

ISR(USART1_RX_vect) {
    UARTS_rx4();
}

// Initialize the TIME1 with CTC mode, interrupt at 1000 Hz
void init_timer();

int main() {
    // ASA_STDIO_init() use UART0, and Atmega88 also have UART0,
    // The configure program is same.
    ASA_DEVICE_set();
    init_timer();
    HAL_time_init();

    sei();

    // m88_init_uartISR_test();

    TypeOfSerialIsr UARTIsrStr = SERIAL_ISR_STR_UART_INI;
    // FIXEME: 使用新的初始化方式
    SerialIsr_init(&UARTIsrStr, 0);

    uint8_t reg_1[1] = {0};
    uint8_t reg_2[2] = {0, 0};
    uint8_t reg_3[3] = {0, 0, 0};
    uint8_t reg_4[1] = {1};
    uint8_t reg_5[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    uint8_t reg_1_Id = RemoRW_reg(&UARTIsrStr, reg_1, 1);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_1_Id,
           UARTIsrStr.remo_reg[reg_1_Id].sz_reg);
    uint8_t reg_2_Id = RemoRW_reg(&UARTIsrStr, reg_2, 2);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_2_Id,
           UARTIsrStr.remo_reg[reg_2_Id].sz_reg);
    uint8_t reg_3_Id = RemoRW_reg(&UARTIsrStr, reg_3, 3);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_3_Id,
           UARTIsrStr.remo_reg[reg_3_Id].sz_reg);
    uint8_t reg_4_Id = RemoRW_reg(&UARTIsrStr, reg_4, 1);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_4_Id,
           UARTIsrStr.remo_reg[reg_4_Id].sz_reg);
    uint8_t reg_5_Id = RemoRW_reg(&UARTIsrStr, reg_5, 10);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_5_Id,
           UARTIsrStr.remo_reg[reg_5_Id].sz_reg);

    while (1) {
        // Show the register data
        printf(
            "reg_1[0] = %3u,reg_2[0] = %3u,reg_2[1] = %3u,reg_3[0] = "
            "%3u,reg_3[1] = %3u,reg_3[2] = %3u\n",
            reg_1[0], reg_2[0], reg_2[1], reg_3[0], reg_3[1], reg_3[2]);
        HAL_delay(100UL);
    }
}

void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR1A  [COM1A1 COM1A0 COM1B1 COM1B0 COM1C1 COM1C0 WGM11 WGM10]
    // TCCR1B  [ICNC1 ICES1 -- WGM13 WGM12 CS12 CS11 CS10]

    ICR1 = 11058;
    TCCR1A = 0b00000000;
    TCCR1B = 0b00011001;
    TCNT1 = 0x00;
    TIMSK |= 1 << OCIE1A;
}
