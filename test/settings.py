import configparser
import ast
import os

__ALL__ = ['CFG']

def read_compiler(cfg, compiler):
    c = dict()
    c['name'] = cfg.get(compiler, 'name')
    c['path'] = cfg.get(compiler, 'path')
    return c

def read_device(cfg, device):
    d = dict()
    d['name'] = cfg.get(device, 'name')
    d['platform'] = cfg.get(device, 'platform')
    d['mcu']  = cfg.get(device, 'mcu')
    d['fcpu'] = cfg.get(device, 'fcpu')
    return d

def parse_sets(file):
    res = dict()
    cfg = configparser.ConfigParser()
    cfg.read(file)

    res['version'] = cfg.get('General', 'version')
    res['library_path'] = cfg.get('General', 'library_path')
    res['modules'] = ast.literal_eval(cfg.get('General', 'modules'))

    res['devices'] = list()
    for device in ast.literal_eval(cfg.get('General', 'devices')):
        res['devices'] += [read_device(cfg, device)]

    res['avr_compilers'] = list()
    for compiler in ast.literal_eval(cfg.get('General', 'avr_compilers')):
        res['avr_compilers'] += [read_compiler(cfg, compiler)]

    res['arm_compilers'] = list()
    for compiler in ast.literal_eval(cfg.get('General', 'arm_compilers')):
        res['arm_compilers'] += [read_compiler(cfg, compiler)]

    return res


_DEF_SETFILE = 'scripts/settings.ini'
_USER_SETFILE = 'scripts/settings.user.ini'

if os.path.isfile(_DEF_SETFILE):
    SETTINGS = parse_sets(_DEF_SETFILE)
elif os.path.isfile(_USER_SETFILE):
    SETTINGS = parse_sets(_USER_SETFILE)
else:
    raise("can't find setting file")

def run():
    print(SETTINGS)
    
if __name__ == "__main__":
    run()
