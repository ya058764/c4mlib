import os
import sys
import glob
import time
import shutil
import zipfile
import configparser
import ast
import subprocess

from settings import SETTINGS

VERSION = SETTINGS['version']
# TODO 更改成不相依編譯器編號
ZIPFILE = "dist/" + "c4mlib_" + VERSION + "_asam128_avr-gcc-8.2.0.zip"

def run():
    print(ZIPFILE)
    with zipfile.ZipFile(ZIPFILE, 'r') as zip:
        zip.extractall(path='test/build_test')
    
    os.chdir('test/build_test')
    res = b''
    if sys.platform =='win32':
        with subprocess.Popen(["make", "main.hex"],shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
            res += proc.stdout.read()
            res += proc.stderr.read()
    elif sys.platform == 'linux':
        with subprocess.Popen(["make main.hex"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
            res += proc.stdout.read()
            res += proc.stderr.read()

    print(res.decode('utf-8'))



if __name__ == "__main__":
    run()
