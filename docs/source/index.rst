.. c4mlib documentation master file, created by
   sphinx-quickstart on Mon Dec  3 15:55:43 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to C4MLIB's documentation!
===================================

.. toctree::
   :maxdepth: 2

   modules/index.rst
   develop/index.rst
   dev-note/index.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. toctree::
   :caption: API REFERENCE
   :maxdepth: 2
