api_and_macro
*************

函式介面
========

.. doxygengroup:: hmi
   :project: c4mlib
   :content-only:

HMI資料型態編號macro
====================

.. doxygengroup:: hmi_type_macro
   :project: c4mlib
   :content-only:
