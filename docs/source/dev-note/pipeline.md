# c4mlib 的 CI/CD

- 主寫者： `LiYu87 <https://gitlab.com/mickey9910326>`  
- 編輯者：  
- 日期： 2019.07.17  

## what is CI/CD and pipeline

在gitlab或其他地方會常常看到 **CI/CD pipeline** 這個名詞，雖然CI/CD與pipline常常一起出現，但其實是兩件事情， **CI/CD** 指的是 **Continuous integration(持續整合)** 與 **(Continuous delivery)持續交付** 或 **Continuous deployment(持續部屬)** ，是在描述軟體開發循環中的一部分流程，而 **pipeline** 單純是指一連串的工作，整個 **CI/CD pipeline** 的意思就是 **CI/CD** 相關的一連串動作。

這一串動作通常有，自動測試、自動組建、自動部屬等工作，主要目的是為了加快開發與釋出這個循環，這種概念目前比較常用在web相關領域的軟體開發中，而在近年，因為許多免費、開源工具出現，像Docker，所以這類的概念越來越容易實現，也逐漸有的專門做這件事情的職位(DevOps)。

可以看下方連結的基本介紹：

- [wiki CI](https://zh.wikipedia.org/wiki/持續整合)

- [wiki CD](https://zh.wikipedia.org/wiki/持續交付)

- [AWS 什麼是持續整合？](https://aws.amazon.com/tw/devops/continuous-integration/)

- [一篇文了解DevOps](https://www.jianshu.com/p/ef11f0326394)
推薦，這篇描述了一些DevOps及這些技術名詞出現的背景原因及歷史。

- [敏捷开发、持续集成/交付（CI/CD）、DevOps学习笔记](https://blog.csdn.net/CrankZ/article/details/81545439)

- [什麼是 CI / CD ?](https://medium.com/@william456821/%E4%BB%80%E9%BA%BC%E6%98%AF-ci-cd-72bd5ae571f1)

## 什麼時候會觸發 CI/CD pipeline，它做了什麼

實現相關細節可以參考 [GitLab CI/CD](https://docs.gitlab.com/ee/ci/README.html)。

以下描述都是以c4mlib的環境與開發來說明(2019.07.17 8c875f507c66ce7766bf4e904c8f87e603e59617)。

目前會觸發 pipeline 的是每個commit及merge前後，在每個 commit 後 pipeline 都會做兩件事情，一是編譯測試，目前只包含了能否以 avr-gcc-8.1.0 的編譯器產生出 c4mlib.a 及 c4mlib.h ，二是函式庫應用測試，目前只包含了測試一個空的main function加上函式庫能否正常編譯，主要是測試.h檔的自動生成(以python腳本實現)有沒有出錯。

而測試完成後會有標籤說明這次測試有沒有通過，如下兩張圖。透過這個pipeline，可以大幅減少主要維護人員的測試時間，而開發人員也有個很醒目的提示，確認自己在這次commit中有沒有出錯。

![passsed](../_static/commit-passed.png)  

![fail](../_static/commit-fial.png)  

## CI/CD pipeline 怎麼實現

就 c4mlib 這個專案而言，他用了以下幾種技術或框架：  

- docker
- python
- shell script
- build complier toolchain

gitlib 的 pipeline 是由`.gitlab-ci.yml`所管理的，可以在[GitLab CI/CD Pipeline Configuration Reference](https://gitlab.com/help/ci/yaml/README)中找到詳細說明。以下為目前c4mlib中的內容：

``` yaml
image: mickey9910326/avr-gcc-docker
# 使用的環境
# 目前使用的docker環境為 mickey9910326/avr-gcc-docker
# 是一個有avr-gcc-8.1.0編譯器及python3.7的類Unix系統
# https://github.com/mickey9910326/avr-gcc-docker

stages:
  - build
  # 要執行的階段性工作
  # 目前只有一個build，包含函式庫組建測試、及函式庫應用測試。

build-test: # 為一個 Job 名稱為 build-test，會在階段 build 執行
  stage: build 
  script: # Job 要執行的腳本，為 shell script
    - python3 scripts/build.py && python3 test/build_test.py
    # python3 scripts/build.py 是 函式庫組建測試
    # python3 test/build_test.py 是 函式庫應用測試
```

目前兩個測試的 python 腳本內容都過於簡單又少，所以不做說明，若未來有更多的測試腳本及測試內容，再考慮在此份文件中說明。

## 未來改進方向

- 目前 docker 環境，只有avr-gcc-8.1.0的環境，而且是在類unix系統上，之前有嘗試用 windows 的 container 去做，但環境有點難架起來，所以作罷了。並希望能有更多版本的環境測試，像winavr的gnu 4.x的版本，或as7使用的gnu5.x版本的環境，來做更全面的測試，

- 目前 docker 環境是掛在個人專案下的，或許移進團隊中比較好，但個人是希望有其他貢獻者出現再移動到實驗室的團隊中。

- 測試項目過少，目前測試項目只有"函式庫組建測試"及"函式庫應用測試"，而且函式庫組建測試只有測試能不能產出.a，沒有測試個模組的測試程式能不能編譯成功。

- 缺少自動釋出的腳本，目前釋出的方式仍能是在個人的電腦上組建完成後再手動釋出，希望能更自動化一點，但卡在 windows 的 container 難以架設。
