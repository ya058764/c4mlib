import os

def genC4mlibArchive(sets, device, compiler):
    libpath = sets['library_path']
    modules = sets['modules']
    
    cmd = 'make -s -C ' + libpath + ' '
    cmd += 'MODULES=' + '\"' + ' '.join(modules) + '\" '
    cmd += 'COMPILER_PATH={} MCU={} FCPU={}'.format(
        compiler['path'],
        device['mcu'],
        device['fcpu']
    )
    print('  start run command:' + cmd)
    os.system(cmd)
